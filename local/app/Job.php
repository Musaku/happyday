<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table = 'jobs';
    protected $fillable = ['job_status','job_no','job_date',
    'quoation_no','quoation_date','cust_no','cust_name','cust_code','po_no','po_revdate','due_po','duedate_po',
    'active','update_date','update_by','record_date','record_by'];
}
