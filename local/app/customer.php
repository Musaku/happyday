<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{
    protected $table = 'customers';
    protected $fillable = ['name','id_cus','cus_no','cus_address','tel','fax','active'];
}
