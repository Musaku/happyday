<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class employee extends Authenticatable
{
    protected $table = 'employees';
    protected $fillable = ['name','lastname','code','name_eng','department','star_work','password','active'];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
