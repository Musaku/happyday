<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class item extends Model
{
    protected $table = 'items';
    protected $fillable = ['job_no','item_no','item_all','item_status','order_qty','cust_no'
    ,'order_unit','qty_set','qty_unit','drawing_no','rev','discirption_1','discirption_2'
    ,'material','hardness','invoice_no','invoice_date','finish_date','temp_no','assign_date'
    ,'assign_by','remark','wip_remark','active','record_by','record_date','update_by','update_date','item_fullname'];
}
