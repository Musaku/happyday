<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\employee;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function check(){
        if (Auth::check()) {
            return true;
        }
        else{
            return false;
        }
    }

    public function index()
    {
        return view('Backend.admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($this->check()) {
            $name = Auth::user()->code;
            $data = array(
            'name' => $name,
        );
        return view('Frontend.employee.add_employee',$data);
        }else{
            return view('layouts.login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'lastname' => 'required',
            'nameeng' => 'required',
            'code' => 'required',
            'department' => 'required',
            'datestart' => 'required',
        ]);
        $pass = '1234' ;
        $employee = new employee;
        $employee->name = $request->name;
        $employee->lastname = $request->lastname;
        $employee->name_eng = $request->nameeng;
        $employee->code = $request->code;
        $employee->department = $request->department;
        $employee->start_work = $request->datestart;
        $employee->password = bcrypt($pass);
        $employee->password_old = $pass;
        $employee->active = '1';
        $employee->save();
        return redirect()->action('AdminController@show');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        if ($this->check()) {
        $name = Auth::user()->code;
        $employee = employee::where('active',1)->orderBy('created_at','desc')->get();
        $data = array(
            'employee' => $employee,
            'name' => $name,
        );
        return view('Frontend.employee.list_employee',$data);
        }else{
            return view('layouts.login');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($this->check()) {
        $name = Auth::user()->code;
        $employee = employee::find($id);
        $data = array(
            'employee' => $employee,
            'name' => $name,
        );
        return view('Frontend.employee.add_employee',$data);
        }else{
            return view('layouts.login');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'lastname' => 'required',
            'nameeng' => 'required',
            'code' => 'required',
            'department' => 'required',
            'datestart' => 'required',
        ]);
        $employee = employee::find($id);
        $employee->name = $request->name;
        $employee->lastname = $request->lastname;
        $employee->name_eng = $request->nameeng;
        $employee->code = $request->code;
        $employee->department = $request->department;
        $employee->start_work = $request->datestart;
        $employee->active = '1';
        $employee->save();
        return redirect()->action('AdminController@show');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = employee::find($id);
        $employee->active = '0';
        $employee->save();
        // $employee->delete();
        Session::flash('message', 'Success Delete !!');
        return redirect()->action('AdminController@show');
    }

    public function editpass($id){
        if ($this->check()) {
        $name = Auth::user()->code;
        $employee = employee::find($id);
        $data = array(
            'employee'=>$employee,
            'name' => $name,
        );
            return view('frontend.employee.resetpass_employee',$data);
        }else{
            return view('layouts.login');
        }

    }

    public function resetpass(Request $request, $id){
        
        if ($this->check()) {
            $name = Auth::user()->code;
            $employee = employee::find($id);
            if ($request->oldpass == $employee->password_old) {
                    if ($request->newpass == $request->conpass) {
                        $employee->password = bcrypt($request->newpass);
                        $employee->password_old = $request->newpass;
                        $employee->save();
                        return redirect()->action('AdminController@show');
                        exit();
                    } else {
                         $data = array(
                        'employee'=>$employee,
                        'name' => $name,
                        'not' => 2,
                        );
                        return view('frontend.employee.resetpass_employee',$data);
                    }
            }else{
                $data = array(
                'employee'=>$employee,
                'name' => $name,
                'not' => 1,
                );
                return view('frontend.employee.resetpass_employee',$data);
            }
        }else{
            return view('layouts.login');
        }

    }
}
