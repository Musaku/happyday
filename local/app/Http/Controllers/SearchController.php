<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\customer;
use App\job;
use App\item;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
   public function search_order(Request $request)
   {
        $newitem = explode('/',$request->item);
        $data = DB::table('jobs')
                ->Join('items', 'jobs.job_no', '=', 'items.job_no')
                ->select('jobs.id','jobs.cust_name','jobs.po_no','jobs.job_date','jobs.duedate_po','jobs.job_status','jobs.job_no as job_no_job',
                'items.job_no as job_no_item','items.item_no','items.item_type','items.drawing_no','items.rev','items.discirption_1',
                'items.order_qty','items.qty_set','items.remark','items.item_status','items.material','items.item_fullname'
                ,DB::raw('CONCAT(items.item_no,"/",items.item_type) as full_item'))
                ->where('jobs.active',1);
                // job////
                if (!empty($request->custname)){
                        $data->where('jobs.cust_name', 'like', '%'.$request->custname.'%');
                }
                if (!empty($request->ponumber)){
                        $data->where('jobs.po_no', 'like', '%'.$request->ponumber.'%');
                }
                if (!empty($request->jobno)){
                        $data->where('jobs.job_no', 'like', '%'.$request->jobno.'%');
                }
                if (!empty($request->opendate)){
                        $data->where('jobs.job_date', 'like', '%'.$request->opendate.'%');
                }
                if (!empty($request->duedate)){
                        $data->where('jobs.duedate_po', 'like', '%'.$request->duedate.'%');
                }
                if (!empty($request->statusporcess)){
                        $data->where('jobs.job_status', 'like', '%'.$request->statusporcess.'%');
                }
                // item////
                if (!empty($request->item)){
                        $data->where('items.item_fullname', 'like', '%'.$request->item.'%');
                }
                if (!empty($request->partname)){
                        $data->where('items.discirption_1', 'like', '%'.$request->partname.'%');
                }
                if (!empty($request->drawing)){
                        $data->where('items.drawing_no', 'like', '%'.$request->drawing.'%');
                }
                if (!empty($request->rev)){
                        $data->where('items.rev', 'like', '%'.$request->rev.'%');
                }
                if (!empty($request->orderqty)){
                        $data->where('items.order_qty', 'like', '%'.$request->orderqty.'%');
                }
                if (!empty($request->stockbalance)){
                        $data->where('items.qty_set', 'like', '%'.$request->stockbalance.'%');
                }
                if (!empty($request->material)){
                        $data->where('items.material', 'like', '%'.$request->material.'%');
                }
                if (!empty($request->partstatus)){
                        $data->where('items.item_status', 'like', '%'.$request->partstatus.'%');
                }
                $result = $data->get();
                $name = Auth::user()->code;
                $data = array(
                        'name' => $name,
                        'result' => $result
                        );
                return view('Frontend.searchmain',$data);
   }

   public function show($id)
   {
        $new = base64_decode($id);
        $newcode2 = explode('@',$new,2);
        $job = DB::table('jobs')
        ->leftjoin('items', 'jobs.job_no', '=', 'items.job_no')
        ->select('jobs.id as id_job','jobs.*','items.id as id_item','items.*')
        ->where('items.job_no',$newcode2[0])
        ->where('items.item_fullname',$newcode2[1])
        ->where('jobs.active',1)
        ->where('items.active',1)
        ->get();
        $items = item::where('job_no',$job[0]->job_no)->where('active',1)->get();
        $name = Auth::user()->code;
            $data = array(
            'name' => $name,
            'jobs' => $job,
            'job_print' => $job,
            'item_show' => $items
            );
        return view('Frontend.joborder',$data);
   }
}
