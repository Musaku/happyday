<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function check(){
        if (Auth::check()) {
            return true;
        }
        else{
            return false;
        }
    }

    public function index()
    {
        if ($this->check()) {
            $name = Auth::user()->code;
            $data = array(
            'name' => $name,
        );
            return view('Frontend.index',$data);
        }else{
            return view('layouts.login');
        }
        
    }
    
    public function searchmain()
    {
        if ($this->check()) {
            $name = Auth::user()->code;
            $data = array(
            'name' => $name,
        );
        return view('Frontend.searchmain',$data);
        }else{
            return view('layouts.login');
        }
    }
    public function reportjob()
    {
        if ($this->check()) {
            $name = Auth::user()->code;
            $data = array(
            'name' => $name,
        );
        return view('Frontend.reportjob',$data);
        }else{
            return view('layouts.login');
        }
    }
}