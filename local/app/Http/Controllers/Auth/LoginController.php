<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\employee;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('guest')->except('logout');
    // }
    public function login(Request $request){
        
        if (Auth::guard('Login')->attempt(['code' => $request->input('code'), 'password' => $request->input('password') ])) {
            $name = Auth::user()->code;
            $data = array(
            'name' => $name,
        );
            return view('Frontend.index',$data);
        } else {
            $error = 1;
            $data = array(
            'error' => $error,
            );
            return view('layouts.login',$data);
        }
        
    }

    public function logout(){
        Auth::logout();
        return view('layouts.login');
        
    }
}
