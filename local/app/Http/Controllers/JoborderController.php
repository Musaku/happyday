<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\customer;
use App\product;
use App\Job;
use App\item;
use Datatables;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
class JoborderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function check(){
        if (Auth::check()) {
            return true;
        }
        else{
            return false;
        }
    }

    public function joborder()
    {
        if ($this->check()) {
            $name = Auth::user()->code;
            $data = array(
            'name' => $name,
        );
            return view('Frontend.joborder',$data);
        }else{
            return view('layouts.login');
        }
        
    }

public function create_first(Request $request)
    {
            $this->validate($request,[
                // 'job_status' => 'required',
                'cust_no' => 'required',
                'cust_code' => 'required',
                'cust_name' => 'required',
                'job_no' => 'required',
                'job_date' => 'required',
                // 'quoation_no' => 'required',
                // 'quoation_date' => 'required',
            ]);
            $job = new job;
            $job->job_status = $request->job_status;
            $job->cust_no = $request->cust_no;
            $job->cust_code = $request->cust_code;
            $job->cust_name = $request->cust_name;
            $job->job_no = $request->job_no;
            $job->job_date = $request->job_date;
            $job->quoation_no = $request->quoation_no;
            $job->quoation_date = $request->quoation_date;
            $name = Auth::user()->name;
            $job->record_date = Carbon::now()->format('d/m/Y H:i:s');
            $job->record_by = $name;
            $job->update_date = Carbon::now()->format('d/m/Y H:i:s');
            $job->update_by = $name;
                if (isset($request->po_no)) {
                    $job->po_no = $request->po_no;
                    $job->po_revdate = $request->po_recdate;
                    $job->due_po = $request->due_po;
                    $job->duedate_po = $request->duedate_po;
                }
            $job->active = 1 ;
            $job->save();
            if (isset($request->qty)) {
                $this->validate($request,[
                    'item_no' => 'required',
                    'item_status' => 'required',
                    'qty' => 'required',
                    'qty_unit' => 'required',
                ]);
                $item = new item;
                $item->job_no = $request->job_no;
                $newitem1 = explode('/',$request->item_no);
                $item->item_no = $newitem1[0];
                $item->item_type = $newitem1[1];
                $item->item_fullname = $request->item_no;
                $item->item_status = $request->item_status;
                $item->order_qty = $request->qty;
                $item->order_unit = $request->qty_unit;
                $item->drawing_no = $request->draw;
                $item->rev = $request->rev;
                $item->qty_set = $request->qty_set;
                $item->qty_unit = $request->qty_set_unit;
                $item->discirption_1 = $request->discrip1;
                $item->discirption_2 = $request->discrip2;
                $item->material = $request->material;
                $item->hardness = $request->hardness;
                $item->invoice_no = $request->in_no;
                $item->invoice_date = $request->in_date;
                $item->temp_no = $request->temp;
                if ($request->item_status == 'Finish') {
                    $item->finish_date = Carbon::now()->format('d/m/Y H:i:s');
                }
                $item->remark = $request->remark;
                $item->wip_remark = $request->wip_remark;
                $item->active = 1;
                $item->save();
                $newitem1 = explode('/',$request->item_no);
                $plus = item::where('item_type','!=',$newitem1[1])->where('job_no',$request->job_no)->where('active',1)->get();
                foreach ($plus as $key => $value2) {
                    $value2->item_type = $newitem1[1];
                    $value2->item_fullname = $value2->item_no.'/'.$newitem1[1];
                    $value2->save();
                }
            }
        $item_2 = item::where('job_no',$request->job_no)->where('active',1)->get();
        $itemCount = count($item_2)+1;
        $item_no = $itemCount.'/'.$itemCount;
        $item2 = item::where('job_no',$request->job_no)->where('active',1)->get();
        $name = Auth::user()->code;
        $name_login = Auth::user()->name;
            $data = array(
            'name' => $name,
            'job' => $job,
            'item_no' => $item_no,
            'item_show' => $item2,
            'name_login' => $name_login
        );
        return view('Frontend.joborder',$data);
    }

    public function update_job(Request $request, $id)
    {
        $this->validate($request,[
            // 'job_status' => 'required',
            'cust_no' => 'required',
            'cust_code' => 'required',
            'cust_name' => 'required',
            'job_no' => 'required',
            'job_date' => 'required',
            // 'quoation_no' => 'required',
            // 'quoation_date' => 'required',
        ]);
        $job = job::find($id);
        $job->job_status = $request->job_status;
        $job->cust_no = $request->cust_no;
        $job->cust_code = $request->cust_code;
        $job->cust_name = $request->cust_name;
        $job->job_no = $request->job_no;
        $job->job_date = $request->job_date;
        $job->quoation_no = $request->quoation_no;
        $job->quoation_date = $request->quoation_date;
        $name = Auth::user()->name;
        // $job->record_date = Carbon::now()->format('d/m/Y H');
        // $job->record_by = $name;
        $job->update_date = Carbon::now()->format('d/m/Y H:i:s');
        $job->update_by = $name;
            if (isset($request->po_no)) {
                $job->po_no = $request->po_no;
                $job->po_revdate = $request->po_recdate;
                $job->due_po = $request->due_po;
                $job->duedate_po = $request->duedate_po;
            }
        $job->active = 1 ;
        $job->update();
            if (isset($request->qty)) {
                $this->validate($request,[
                    'item_no' => 'required',
                    'item_status' => 'required',
                    'qty' => 'required',
                    'qty_unit' => 'required',
                    // 'draw' => 'required',
                    // 'rev' => 'required',
                    // 'qty_set' => 'required',
                    // 'qty_set_unit' => 'required',
                    // 'discrip1' => 'required',
                    // 'discrip2' => 'required',
                    // 'material' => 'required',
                    // 'hardness' => 'required',
                    // 'assing_by' => 'required',
                    // 'in_no' => 'required',
                    // 'in_date' => 'required',
                    // 'as_date' => 'required',
                    // 'temp' => 'required',
                    // 'finish_date'=> 'required',
                    // 'remark' => 'required',
                    // 'wip_remark' => 'required',
                ]);
                $item = new item;
                $item->job_no = $request->job_no;
                $newitem1 = explode('/',$request->item_no);
                $item->item_no = $newitem1[0];
                $item->item_type = $newitem1[1];
                $item->item_fullname = $request->item_no;
                $item->item_status = $request->item_status;
                $item->order_qty = $request->qty;
                $item->order_unit = $request->qty_unit;
                $item->drawing_no = $request->draw;
                $item->rev = $request->rev;
                $item->qty_set = $request->qty_set;
                $item->qty_unit = $request->qty_set_unit;
                $item->discirption_1 = $request->discrip1;
                $item->discirption_2 = $request->discrip2;
                $item->material = $request->material;
                $item->hardness = $request->hardness;
                // $item->assign_by = $request->assing_by;
                $item->invoice_no = $request->in_no;
                $item->invoice_date = $request->in_date;
                // $item->assign_date = $request->as_date;
                $item->temp_no = $request->temp;
                if ($request->item_status == 'Finish') {
                    $item->finish_date = Carbon::now()->format('d/m/Y H:i:s');
                }
                $item->remark = $request->remark;
                $item->wip_remark = $request->wip_remark;
                $item->active = 1;
                $item->save();
                // $item_2 = item::where('job_no',$request->job_no)->where('active',1)->get();
                // $itemCount = count($item_2)+1;
                // $item_no = $itemCount.'/'.$itemCount;
                $newitem1 = explode('/',$request->item_no);
                $plus = item::where('item_type','!=',$newitem1[1])->where('job_no',$request->job_no)->where('active',1)->get();
                foreach ($plus as $key => $value2) {
                    $value2->item_type = $newitem1[1];
                    $value2->item_fullname = $value2->item_no.'/'.$newitem1[1];
                    $value2->save();
                }
                
            }
        $item_2 = item::where('job_no',$request->job_no)->where('active',1)->get();
        $itemCount = count($item_2)+1;
        $item_no = $itemCount.'/'.$itemCount;
        $item2 = item::where('job_no',$request->job_no)->where('active',1)->get();
        $name = Auth::user()->code;
        $name_login = Auth::user()->name;
            $data = array(
            'name' => $name,
            'job' => $job,
            'item_no' => $item_no,
            'item_show' => $item2,
            'name_login' => $name_login
        );
        return view('Frontend.joborder',$data);
        // return redirect()->action('JoborderController@joborder');
    }

    public function update_item(Request $request, $key)
    {
        $id = base64_decode($key);
        $newid = explode('&',$id);
        $this->validate($request,[
            // 'job_status' => 'required',
            'cust_no' => 'required',
            'cust_code' => 'required',
            'cust_name' => 'required',
            'job_no' => 'required',
            'job_date' => 'required',
            // 'quoation_no' => 'required',
            // 'quoation_date' => 'required',
        ]);
        $job = job::find($newid[0]);
        $job->job_status = $request->job_status;
        $job->cust_no = $request->cust_no;
        $job->cust_code = $request->cust_code;
        $job->cust_name = $request->cust_name;
        $job->job_no = $request->job_no;
        $job->job_date = $request->job_date;
        $job->quoation_no = $request->quoation_no;
        $job->quoation_date = $request->quoation_date;
        $name = Auth::user()->name;
        $job->update_date = Carbon::now()->format('d/m/Y H:i:s');
        $job->update_by = $name;
            if (isset($request->po_no)) {
                $job->po_no = $request->po_no;
                $job->po_revdate = $request->po_recdate;
                $job->due_po = $request->due_po;
                $job->duedate_po = $request->duedate_po;
            }
        $job->active = 1 ;
        $job->update();
            if (isset($request->qty)) {
                $this->validate($request,[
                    'item_no' => 'required',
                    'item_status' => 'required',
                    'qty' => 'required',
                    'qty_unit' => 'required',
                    // 'draw' => 'required',
                    // 'rev' => 'required',
                    // 'qty_set' => 'required',
                    // 'qty_set_unit' => 'required',
                    // 'discrip1' => 'required',
                    // 'discrip2' => 'required',
                    // 'material' => 'required',
                    // 'hardness' => 'required',
                    // 'assing_by' => 'required',
                    // 'in_no' => 'required',
                    // 'in_date' => 'required',
                    // 'as_date' => 'required',
                    // 'temp' => 'required',
                    // 'finish_date'=> 'required',
                    // 'remark' => 'required',
                    // 'wip_remark' => 'required',
                ]);
                $item = item::find($newid[1]);
                $item->job_no = $request->job_no;
                $newitem1 = explode('/',$request->item_no);
                $item->item_no = $newitem1[0];
                $item->item_type = $newitem1[1];
                $item->item_fullname = $request->item_no;
                $item->item_status = $request->item_status;
                $item->order_qty = $request->qty;
                $item->order_unit = $request->qty_unit;
                $item->drawing_no = $request->draw;
                $item->rev = $request->rev;
                $item->qty_set = $request->qty_set;
                $item->qty_unit = $request->qty_set_unit;
                $item->discirption_1 = $request->discrip1;
                $item->discirption_2 = $request->discrip2;
                $item->material = $request->material;
                $item->hardness = $request->hardness;
                // $item->assign_by = $request->assing_by;
                $item->invoice_no = $request->in_no;
                $item->invoice_date = $request->in_date;
                // $item->assign_date = $request->as_date;
                $item->temp_no = $request->temp;
                if ($request->item_status == 'Finish') {
                    $item->finish_date = Carbon::now()->format('d/m/Y H:i:s');
                }
                $item->remark = $request->remark;
                $item->wip_remark = $request->wip_remark;
                $item->active = 1;
                $item->update();
                // $item_2 = item::where('job_no',$request->job_no)->where('active',1)->get();
                // $itemCount = count($item_2)+1;
                // $item_no = $itemCount.'/'.$itemCount;
                $newitem1 = explode('/',$request->item_no);
                $plus = item::where('item_type','!=',$newitem1[1])->where('job_no',$request->job_no)->where('active',1)->get();
                foreach ($plus as $key => $value2) {
                    $value2->item_type = $newitem1[1];
                    $value2->item_fullname = $value2->item_no.'/'.$newitem1[1];
                    $value2->save();
                }
                // $count_total = count(item::where('job_no',$request->job_no)->where('active',1)->get());
                // $count_finish = count(item::where('job_no',$request->job_no)->where('active',1)->where('item_status','Finish')->get());
                // if ($count_total == $count_finish) {
                //     $job->job_status = 2;
                //     $job->update();
                // }
            }
        $item_2 = item::where('job_no',$request->job_no)->where('active',1)->get();
        $itemCount = count($item_2)+1;
        $item_no = $itemCount.'/'.$itemCount;
        $item2 = item::where('job_no',$request->job_no)->where('active',1)->get();
        $name = Auth::user()->code;
        $name_login = Auth::user()->name;
            $data = array(
            'name' => $name,
            'job' => $job,
            'item_no' => $item_no,
            'item_show' => $item2,
            'name_login' => $name_login
        );
        return view('Frontend.joborder',$data);
        // return redirect()->action('JoborderController@joborder');
    }

    public function delete($id)
    {
        $job = job::find($id);
        $name_job = $job->job_no;
        $items = item::where('job_no',$job->job_no)->where('active',1)->get();
        foreach ($items as $key => $value) {
            $items[$key]->active = 0;
            $items[$key]->update();
        }
        $job->active = 0;
        $job->update();
        $delete = 1 ;
        $name = Auth::user()->code;
        $data = array(
            'delete' => $delete,
            'name' => $name,
            'name_job' => $name_job
        );
        return $data;
        // return view('Frontend.joborder',$data);
        // return redirect()->action('JoborderController@joborder',$data);
    }

    public function delete_item($id){
        $code = base64_decode($id);
        $new_code = explode("@",$code);
        
        $items = item::where('job_no',$new_code[0])->where('item_fullname',$new_code[1])->where('active',1)->get();
        foreach ($items as $key => $value) {
            // $value->active = 0;
            // $value->update();
            $value->delete();
        }
        
        $reduce = item::where('job_no',$new_code[0])->where('active',1)->get();
        $total = count($reduce);
        foreach ($reduce as $key => $value2) {
            $value2->item_type = $total;
            $value2->item_fullname = $value2->item_no.'/'.$total;
            $value2->update();
        }

        $job_show = DB::table('jobs')
        ->leftjoin('items', 'jobs.job_no', '=', 'items.job_no')
        ->select('jobs.id as id_job','jobs.*','items.id as id_item','items.*')
        ->where('items.job_no',$reduce[0]->job_no)
        ->where('items.item_fullname',$reduce[0]->item_fullname)
        ->where('jobs.active',1)
        ->where('items.active',1)
        ->get();
        $items_show = item::where('job_no',$job_show[0]->job_no)->where('active',1)->get();
        $name = Auth::user()->code;
            $data = array(
            'name' => $name,
            'jobs' => $job_show,
            'item_show' => $items_show
            );
        return view('Frontend.joborder',$data);
    }

    ///////////////////////////////////////// Ajax////////////////////////////////////////////////////////
    public function serach_cus($id)
    {
        $customer = customer::where('id_cus',$id)->select('id_cus','cus_name','cus_no')->get();
        $job = DB::table('jobs')->where('cust_code',$id)->groupBy('job_no')->orderby('created_at')->where('active',1)->get();
        $item = count($job)+1;
        //////// date DB //////////
        $job_date = DB::table('jobs')->orderBy('created_at', 'desc')->get();
        if (count($job_date) != 0) {
            $date_job = substr($job_date[0]->job_no,4,2);
            //////// date now //////////
            $date_now = Carbon::now();
            $new_date = explode("-",$date_now);
            $date = $new_date[1];
            if ($date != $date_job) {
                $item = 1;
            }
        }
        $vpost = str_pad($item,3, "0", STR_PAD_LEFT);
        $jobno = $customer[0]->id_cus.date("ym").$vpost;
        $data = array(
            'customer' => $customer,
            'jobno' => $jobno
        );
        return $data;
    }
    public function serach_dis($id)
    {
        $newcode = explode('@',$id,2);
        $items = product::where('drwing',$newcode[0])
        ->where('active',1)
        ->where('rev',$newcode[1])
        ->get();
        $total = count($items);
        $data = array(
            'items' => $items,
            'total' => $total
        );
        return $data;
    }
    public function serach_show(Request $request)
    {
        $type = request('type');
        $date = request('date');
        $text = request('text');
        $status = request('status');
        $job = DB::table('jobs')
        ->Join('items', 'jobs.job_no', '=', 'items.job_no')
        ->where('items.active',1)
        ->where('jobs.active',1);
        if( !empty($status)){
            if ($status != 0) {
                $job->where('jobs.job_status',$status);
            }
        }
        if( !empty($text)){  
            $job->where('jobs.job_no','like', '%'.$text.'%');
        }
        // if( !empty($date)){  
        //     $job->where('jobs.job_date','like', '%'.$date.'%');
        // }
        if( !empty($type)){  
            if ($type == 1) {
                $job->where('jobs.job_date','like', '%'.$date.'%');
            }elseif($type == 2){
                $job->where('jobs.po_no','like', '%'.$date.'%');
            }elseif($type == 3){
                $job->where('jobs.job_date','like', '%'.$date.'%');
            }else{
                $job->where('jobs.job_date','like', '%'.$date.'%');
            }
        }
        $job->select('jobs.id as id_job','jobs.*','items.id as id_item','items.*');
        $jobs = $job->get();
        $sQuery = Datatables::of($jobs)
        ->addColumn('code', function($data) {
            return base64_encode($data->job_no.'@'.$data->item_fullname);
        })
        ->addColumn('job_status2', function($data) {
                if ($data->job_status == 1) {
                    return 'WorkIn Process';
                }elseif ($data->job_status == 2){
                    return 'Finish';
                }else{
                    return 'Cancle';
                }
        });
            return $sQuery->escapeColumns([])->make(true);
    }
    
}
