<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\customer;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function check(){
        if (Auth::check()) {
            return true;
        }
        else{
            return false;
        }
    }

    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($this->check()) {
            $name = Auth::user()->code;
            $data = array(
            'name' => $name,
        );
        return view('Frontend.customer.add_customer',$data);
        }else{
            return view('layouts.login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'code' => 'required',
            'Cus_ID' => 'required',
            'name' => 'required',
            'Tel' => 'required',
            'Fax' => 'required',
            'address' => 'required',
        ]);
        $customer = new customer;
        $customer->id_cus = $request->code;
        $vpost = str_pad($request->Cus_ID,3, "0", STR_PAD_LEFT);
        $customer->cus_no = $vpost;
        $customer->cus_name = $request->name;
        $customer->tel = $request->Tel;
        $customer->fax = $request->Fax;
        $customer->cus_address = $request->address;
        $customer->active = '1';
        $customer->save();
        return redirect()->action('CustomerController@show');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id){
        if ($this->check()) {
        $name = Auth::user()->code;
        $customer = customer::find($id);
        $data = array(
            'customer' => $customer,
            'name' => $name,
        );
        return view('Frontend.customer.view_customer',$data);
        }else{
            return view('layouts.login');
        }
    }

    public function show()
    {
        if ($this->check()) {
        $name = Auth::user()->code;
        $customer = customer::where('active',1)->orderBy('created_at','desc')->get();
        $data = array(
            'customer' => $customer,
            'name' => $name,
        );
        return view('Frontend.customer.customer',$data);
        }else{
            return view('layouts.login');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($this->check()) {
        $name = Auth::user()->code;
        $customer = customer::find($id);
        $data = array(
            'customer' => $customer,
            'name' => $name,
        );
        return view('Frontend.customer.add_customer',$data);
        }else{
            return view('layouts.login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'code' => 'required',
            'Cus_ID' => 'required',
            'name' => 'required',
            'Tel' => 'required',
            'Fax' => 'required',
            'address' => 'required',
        ]);
        $customer = customer::find($id);
        $customer->id_cus = $request->code;
        $vpost = str_pad($request->Cus_ID,3, "0", STR_PAD_LEFT);
        $customer->cus_no = $vpost;
        $customer->cus_name = $request->name;
        $customer->tel = $request->Tel;
        $customer->fax = $request->Fax;
        $customer->cus_address = $request->address;
        $customer->active = '1';
        $customer->save();
        return redirect()->action('CustomerController@show');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = customer::find($id);
        $customer->active = '0';
        $customer->save();
        // $customer->delete();
        return redirect()->action('CustomerController@show');
    }
}
