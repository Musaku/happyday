<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;
use App\job;
use App\item;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function check(){
        if (Auth::check()) {
            return true;
        }
        else{
            return false;
        }
    }

    public function index()
    {
        //
    }
    public function reportjob(){
        if ($this->check()) {
            $name = Auth::user()->code;
            $data = array(
            'name' => $name,
        );
        return view('Frontend.report_job',$data);
        }else{
            return view('layouts.login');
        }

    }
    public function reportwork(){
        if ($this->check()) {
            $name = Auth::user()->code;
            $data = array(
            'name' => $name,
        );
        return view('Frontend.report_work',$data);
        }else{
            return view('layouts.login');
        }

    }

    public function summary_po(){
         if ($this->check()) {
            $name = Auth::user()->code;
            $data = array(
            'name' => $name,
        );
        return view('Frontend.summary_po',$data);
         }else{
            return view('layouts.login');
        }
    }

    public function summary_finish(){
         if ($this->check()) {
            $name = Auth::user()->code;
            $data = array(
            'name' => $name,
        );
        return view('Frontend.summary_finish',$data);
         }else{
            return view('layouts.login');
        }
    }

    public function finish_good(){
         if ($this->check()) {
            $name = Auth::user()->code;
            $data = array(
            'name' => $name,
        );
        return view('Frontend.finish_good',$data);
         }else{
            return view('layouts.login');
        }
    }

    public function print_job($id){
        $code = base64_decode($id);
        $newcode = explode('@',$code,2);
        $job = DB::table('jobs')
        ->leftjoin('items', 'jobs.job_no', '=', 'items.job_no')
        ->where('items.job_no',$newcode[0])
        ->where('items.item_fullname',$newcode[1])
        ->select('jobs.id as id_job','jobs.*','items.id as id_item','items.*')
        ->get();
        $name = Auth::user()->code;
        $data = array(
            'job' => $job,
            'name' => $name
        );
        return view('Frontend.report_job',$data);
    }

    public function between_report(Request $request){
         $job = DB::table('jobs')
        ->leftjoin('items', 'jobs.job_no', '=', 'items.job_no')
        ->where('jobs.active',1)
        ->where('jobs.job_status',$request->status)
        ->where('items.active',1);
        if ($request->date_type == 3) {
            $job->whereBetween('jobs.job_date', [$request->date_1, $request->date_2]);
        } elseif ($request->date_type == 4) {
            $job->whereBetween('jobs.duedate_po', [$request->date_1, $request->date_2]);
        } elseif($request->date_type == 5) {
            $job->whereBetween('items.finish_date', [$request->date_1, $request->date_2]);
        }
        $job->select('jobs.id as id_job','jobs.*','items.id as id_item','items.*');
        $jobs = $job->get();

        $type =$request->date_type;
        $name = Auth::user()->code;
        $data = array(
            'name' => $name,
            'jobs' => $jobs,
        );
        if ($type == 3 || $type == 4 || $type == 5 ) {
            if ($request->status == 1 || $request->status == 3) {
                return view('Frontend.report_work',$data);
            } else {
                return view('Frontend.finish_good',$data);
            }
        } elseif($request->date_type == 1) {
            return view('Frontend.summary_po',$data);
        } else{
            return view('Frontend.summary_finish',$data);
        }
        exit;
    }

    public function job_as(Request $request)
    {
        $key = $request->print;
        if ($key == 1) {
            $newcode = explode('@',$request->item,2);
            $job = DB::table('jobs')
            ->leftjoin('items', 'jobs.job_no', '=', 'items.job_no')
            ->where('items.job_no',$newcode[0])
            ->select('jobs.id as id_job','jobs.*','items.id as id_item','items.*')
            ->get();
        }else{
            $result = $request->item;
        }
        dd($job);
    }

}
