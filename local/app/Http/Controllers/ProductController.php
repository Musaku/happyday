<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\product;
use Datatables;
use Carbon\Carbon;
use PHPExcel;
use PHPExcel_IOFactory;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function check(){
        if (Auth::check()) {
            return true;
        }
        else{
            return false;
        }
    }

    public function index()
    {
        //
    }

    public function create()
    {
        if ($this->check()) {
            $name = Auth::user()->code;
            $data = array(
            'name' => $name,
        );
        return view('Frontend.product.product_add',$data);
        }else{
            return view('layouts.login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'code' => 'required',
            'drw' => 'required',
            'rev' => 'required',
            'drw_rev' => 'required',
            'dis1' => 'required',
            // 'dis2' => 'required',
        ]);
        $product = new product;
        $product->cust_no = $request->code;
        $product->drwing = $request->drw;
        $product->rev = $request->rev;
        $product->drw_rev = $request->drw_rev;
        $product->dis1 = $request->dis1;
        $product->dis2 = $request->dis2;
        $product->active = '1';
        $product->save();
        return redirect()->action('ProductController@show');
    }

    public function list_product(Request $request)
    {
        $product = DB::table('products')
        ->where('active',1);
        $products = $product->get();
        foreach ($products as $key => $value) {
            $value->no = $key+1;
        }
        $sQuery = Datatables::of($products);
        return $sQuery->escapeColumns([])->make(true);
    }
    public function show()
    {
        if ($this->check()) {
            $name = Auth::user()->code;
            $data = array(
            'name' => $name,
        );
        return view('Frontend.product.product_list',$data);
        }else{
            return view('layouts.login');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($this->check()) {
            $name = Auth::user()->code;
            $product = product::find($id);
            $data = array(
            'name' => $name,
            'product' => $product
        );
        return view('Frontend.product.product_add',$data);
        }else{
            return view('layouts.login');
        }
    }
    
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'code' => 'required',
            'drw' => 'required',
            'rev' => 'required',
            'drw_rev' => 'required',
            'dis1' => 'required',
            // 'dis2' => 'required',
        ]);
        $product = product::find($id);
        $product->cust_no = $request->code;
        $product->drwing = $request->drw;
        $product->rev = $request->rev;
        $product->drw_rev = $request->drw_rev;
        $product->dis1 = $request->dis1;
        $product->dis2 = $request->dis2;
        $product->active = '1';
        $product->save();
        return redirect()->action('ProductController@show');
    }
   
    public function destroy($id)
    {
        $product = product::find($id);
        $product->active = '0';
        $product->update();
        // $customer->delete();
        return redirect()->action('ProductController@show');
    }
    
    public function excel(){
        if ($this->check()) {
            $name = Auth::user()->code;
            $data = array(
            'name' => $name,
        );
        return view('Frontend.product.product_excel',$data);
        }else{
            return view('layouts.login');
        }
    }

    public function import_excel(Request $request)
    {
        $path = $request->file('import_file')->getRealPath();
        try {
        $inputFileType = PHPExcel_IOFactory::identify($path);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($path);
                } catch(Exception $e) {
            die('Error loading file "'.pathinfo($path,PATHINFO_BASENAME).'": '.$e->getMessage());
        }
        $sheet = $objPHPExcel->getSheet(0); 
        $highestRow = $sheet->getHighestRow(); 
        $highestColumn = $sheet->getHighestColumn();
        for ($row = 7; $row <= $highestRow; $row++){ 
            $customer = $sheet->getCellByColumnAndRow(0,$row);
            $drw = $sheet->getCellByColumnAndRow(1,$row);
            $rev = $sheet->getCellByColumnAndRow(2,$row);
            $drw_rev = $sheet->getCellByColumnAndRow(3,$row);
            $dis1 = $sheet->getCellByColumnAndRow(4,$row);
            $dis2 = $sheet->getCellByColumnAndRow(5,$row);
            
            if ($customer != ''&&$drw != ''&&$rev != ''&&$drw_rev != ''&&$dis1 != '') {
                    $product_check = product::where('drw_rev',$drw_rev)->get();
                    $count = count($product_check);
                    if ($count != 0) {
                        $product = product::find($product_check[0]->id);
                    } else {
                        $product = new product;
                    }
                        $product->cust_no = $customer;
                        $product->drwing = $drw;
                        $product->rev = $rev;
                        $product->drw_rev = $drw_rev;
                        $product->dis1 = $dis1;
                        $product->dis2 = $dis2;
                        $product->active = 1;
                        $product->save();
            }
        }
        $com = 1;
        if (Auth::check()) { $name = Auth::user()->username;
            $data = array(
            'name' => $name,
            'com' => $com
        );
        return view('Frontend.product.product_excel',$data);
        }
    }
}
