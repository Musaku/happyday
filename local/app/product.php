<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $table = 'products';
    protected $fillable = ['cust_no','drwing','rev','drw_rev','dis1','dis2','active'];
}
