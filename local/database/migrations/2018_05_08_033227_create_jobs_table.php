<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('job_status');
            $table->string('job_no')->unique();
            $table->string('job_date');
            $table->string('quoation_no');
            $table->string('quoation_date');
            $table->string('cust_no');
            $table->string('cust_code');
            $table->string('cust_name');
            $table->string('po_no');
            $table->string('po_revdate');
            $table->string('due_po');
            $table->string('duedate_po');
            $table->string('status');
            $table->string('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
