<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('job_no');
            $table->string('item_no');
            $table->string('item_type');
            $table->string('item_status');
            $table->string('order_qty');
            $table->string('order_unit');
            $table->string('qty_set');
            $table->string('qty_unit');
            $table->string('drawing_no')->unique();
            $table->string('rev');
            $table->text('discirption_1');
            $table->text('discirption_2');
            $table->string('material');
            $table->string('hardness');
            $table->string('invoice_no');
            $table->string('invoice_date');
            $table->string('finish_date');
            $table->string('temp_no');
            $table->string('assign_date');
            $table->string('assign_by');
            $table->string('remark');
            $table->string('wip_remark');
            $table->string('active');
            $table->string('record_by');
            $table->string('record_date');
            $table->string('update_by');
            $table->string('update_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
