@include('layouts.menu')
<style type="text/css">
	.form-control-no{
		padding: .375rem .75rem;
		font-size: 1rem;
		line-height: 1.5;
		color: #495057;
		background-color: #fff;
		background-clip: padding-box;
		border: 1px solid #ced4da;
		border-radius: .25rem;
		transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
	}
	.but{
		height: 40px;
		width: 81px;
	}
	.hd_danger{
	  border-color: red!important;
	}
	table, td, th{
            padding: 5px; 
            padding-left: 10px;
            padding-right: 10px;
            border-collapse: collapse;
        }
</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script language="JavaScript">
	document.onkeydown = chkEvent 
	var formInUse = false;
	function chkEvent(e) {
		var keycode;
		if (window.event) keycode = window.event.keyCode; //*** for IE ***//
		else if (e) keycode = e.which; //*** for Firefox ***//
		if(keycode==13)
		{
			return false;
		}
	}
</script>
<body>
	@if (isset($delete))
		<script>
			swal({
					title: "ลบข้อมูล Job",
					text: "ระบบลบข้อมูล Job {{ $name_job }} เรียบร้อยแล้ว",
					icon: "success",
					successMode: true,
				})
		</script>
	@endif
<div class="container">
	<br>
		<div class="card">
			<div class="card-header bg-secondary text-white">
				Job Order
			</div>
			@if (isset($job))
				{{ Form::open(['url'=>['joborder/update',$job->id],'id'=>'job_form']) }}
			@elseif (isset($jobs))
				{{ Form::open(['url'=>['joborder/update2',base64_encode($jobs[0]->id_job.'&'.$jobs[0]->id_item)],'id'=>'job_form']) }}
			@else
				{{ Form::open(['url'=>'joborder/create','id'=>'job_form']) }}
			@endif
			<div class="card-body">
				@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<div class="card-body card border-secondary mb-3">
					<div class="row">
						<div class="col-md-1">
							<br>
							<a href="{{url('joborder')}}">
								<button type="button" class="btn btn-secondary far fa-file-alt but"><p style="font-size:1px">เปิด Job</p></button>
							</a>
						</div>
						<div class="col-md-1">
							<br>
							<button type="button" class="btn btn-secondary fas fa-search but" data-toggle="modal" data-target="#exampleModal">
								<p style="font-size:1px;text-aline">ค้นหา</p>
							</button>
						</div>
						@if (isset($job))
							<div class="col-md-1">
								<br>
								{{-- <a href="{{url('joborder/delete/'.$job->id )}}"> --}}
									<button type="button" id="delete" value="{{ $job->id }}" class="btn btn-secondary fas fas fa-trash-alt but"><p style="font-size:1px">ลบ Job</p></button>
								{{-- </a> --}}
							</div>
						@elseif(isset($jobs))
							<div class="col-md-1">
								<br>
								{{-- <a href="{{url('joborder/delete/'.$jobs[0]->id_job )}}"> --}}
									<button type="button" id="delete" value="{{ $jobs[0]->id_job }}" class="btn btn-secondary fas fas fa-trash-alt but"><p style="font-size:1px">ลบ Job</p></button>
								{{-- </a> --}}
							</div>
						@else
							<div class="col-md-1">
								<br>
								<button type="button" class="btn btn-secondary fas fas fa-trash-alt but"><p style="font-size:1px">ลบ Job</p></button>
							</div>
						@endif
						<div class="col-md-1">
							@if (isset($job_print))
								<br>
								<button type="button" class="btn btn-secondary fas fa-print but" data-toggle="modal" data-target="#Modal_print">
									<p style="font-size:1px;text-aline">Report</p>
								</button>
								{{-- <a href="{{url('report/print_job/'.base64_encode($job_print[0]->job_no.'@'.$job_print[0]->item_fullname) )}}">
									<button type="button" class="btn btn-secondary fas fa-print but"><p style="font-size:1px">Report</p></button>
								</a> --}}
							@else
								<br>
								<button type="button" class="btn btn-secondary fas fa-print but"><p style="font-size:1px">Report</p></button>
							@endif
						</div>
						<div class="col-md-1">
						</div>
						<div class="col-md-5">
							    
						</div>
						@php
							$status_list = array(
								'1' => 'WorkIn Process',
								'2' => 'Finish',
								'3' => 'Cancel',
							);
						@endphp
						<div class="col-md-2">
							<label for="">Job Status</label>
							<select class="form-control" name="job_status">
								@php
									if (isset($job)) {
										foreach ($status_list as $key => $value) {
											if ($job->job_status == $key) {
												echo '<option selected value='.$key.'>'.$value.'</option>';
											} else {
												echo '<option value='.$key.'>'.$value.'</option>';
											}
										}
									} elseif (isset($jobs)) {
										foreach ($status_list as $key => $value) {
											if ($jobs[0]->job_status == $key) {
												echo '<option selected value='.$key.'>'.$value.'</option>';
											} else {
												echo '<option value='.$key.'>'.$value.'</option>';
											}
										}
									} else {
										echo '<option selected value=1>WorkIn Process</option>' ;
										echo '<option value=2>Finish</option>' ;
										echo '<option value=3>Cancle</option>' ;
									}
								@endphp	
							</select>
						</div>
					 </div>
				</div>
				<div class="card-body card border-secondary  mb-3 col-md-12">
					<div class="row">
						<div class="col-md-3">
							<label for="">Cust Code</label>
							<br>
							<div class="input-group mb-3">
								@if (isset($job))
									<input type="text" onKeyDown="if(event.keyCode==13){ quoation_no.focus()}" value="{{ $job->cust_code }}" id="cus_code" name="cust_code" class="form-control" id="" placeholder="Cust Code" maxlength="4" >
								@elseif (isset($jobs))
									<input type="text" onKeyDown="if(event.keyCode==13){ quoation_no.focus()}" value="{{ $jobs[0]->cust_code }}" id="cus_code" name="cust_code" class="form-control" id="" placeholder="Cust Code" maxlength="4">
								@else
									<input type="text" onKeyDown="if(event.keyCode==13){ quoation_no.focus()}" id="cus_code" name="cust_code" class="form-control" id="" placeholder="Cust Code" maxlength="4" >
								@endif
								{{-- <div class="input-group-append">
									<button class="fas fa-search" id="search_cus" type="button" style="width: 50px"></button>
								</div> --}}
							</div>
						</div>
						<div class="col-md-3">
							<label for="">Cust ID</label>
							<br>
								@if (isset($job))
									<input type="text" value="<?php echo $job->cust_no ?>" class="form-control-no" name="cust_no" id="cus_id" placeholder="Cust ID" style="width: 100%;" maxlength="4">
								@elseif (isset($jobs))
									<input type="text" value="<?php echo $jobs[0]->cust_no ?>" class="form-control-no" name="cust_no" id="cus_id" placeholder="Cust ID" style="width: 100%;" maxlength="4" >
								@else
									<input type="text" class="form-control-no" name="cust_no" id="cus_id" placeholder="Cust ID" style="width: 100%;" maxlength="4" >
								@endif
						</div>
						<div class="col-md-6">
							<label for="">Cust Name</label>
							@if (isset($job))
								<input type="text" value="{{ $job->cust_name }}" id="cust_name" name="cust_name" class="form-control" id="" placeholder="Cust. Name">
							@elseif(isset($jobs))
								<input type="text" value="{{ $jobs[0]->cust_name }}" id="cust_name" name="cust_name" class="form-control" id="" placeholder="Cust. Name">
							@else
								<input type="text" id="cust_name" name="cust_name" class="form-control" id="" placeholder="Cust. Name">
							@endif
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<label for="">Job No.</label>
							@if (isset($job))
								<input type="text" value="{{ $job->job_no }}" id="job_no" class="form-control" name="job_no" id="" placeholder="Job No">
							@elseif(isset($jobs))
								<input type="text" value="{{ $jobs[0]->job_no }}" id="job_no" class="form-control" name="job_no" id="" placeholder="Job No">
							@else
								<input type="text" id="job_no" class="form-control" name="job_no" id="" placeholder="Job No">
							@endif
							
						</div>
						{{-- <div class="col-md-3 "> --}}
							<div class="col-md-3">
									<label for="">Job Date</label>
										@if (isset($job))
											{{-- <div class="ui calendar" id=""> --}}
											<div class="ui input left icon" style="width: 100%;">
											<i class="calendar icon"></i>
											<input type="text" readonly value="{{ $job->job_date }}" placeholder="Date" name="job_date">
										@elseif(isset($jobs))
											{{-- <div class="ui calendar" id=""> --}}
											<div class="ui input left icon" style="width: 100%;">
											<i class="calendar icon"></i>
											<input type="text" readonly value="{{ $jobs[0]->job_date }}" placeholder="Date" name="job_date">
										@else
											{{-- <div class="ui calendar example2" id=""> --}}
											<div class="ui input left icon" style="width: 100%;">
											<i class="calendar icon"></i>
											<input type="text" readonly value="{{ date('d M,Y') }}" placeholder="Date" name="job_date">
										@endif
									</div>
								{{-- </div> --}}
							</div>
							<div class="col-md-3">
									<label for="">Quoation No.</label>
									@if (isset($job))
										<input type="text" onKeyDown="if(event.keyCode==13){ quoation_date.focus()}" value="{{ $job->quoation_no }}" class="form-control" name="quoation_no" id="" placeholder="Quoation No">
									@elseif(isset($jobs))
										<input type="text" onKeyDown="if(event.keyCode==13){ quoation_date.focus()}" value="{{ $jobs[0]->quoation_no }}" class="form-control" name="quoation_no" id="" placeholder="Quoation No">
									@else
										<input type="text" onKeyDown="if(event.keyCode==13){ quoation_date.focus()}" class="form-control" name="quoation_no" id="" placeholder="Quoation No">
									@endif
							</div>
							<div class="col-md-3">
								<label for="">Quoation  Date</label>
									<div class="ui calendar example2" id="">
										<div class="ui input left icon" style="width: 100%;">
											<i class="calendar icon"></i>
											@if (isset($job))
												<input type="text" onKeyDown="if(event.keyCode==13){ po_no.focus()}" value="{{ $job->quoation_date }}" placeholder="Date" name="quoation_date">
											@elseif(isset($jobs))
												<input type="text" onKeyDown="if(event.keyCode==13){ po_no.focus()}" value="{{ $jobs[0]->quoation_date }}" placeholder="Date" name="quoation_date">
											@else
												<input type="text" onKeyDown="if(event.keyCode==13){ po_no.focus()}" placeholder="Date" name="quoation_date">
											@endif
										</div>
									</div>
							</div>
						{{-- </div> --}}
					 </div>
					 <br>
					 <div class="row">
					 	<div class="col-md-3">
							 <label for="">P/O No.</label>
							@if (isset($job))
								 <input type="text" onKeyDown="if(event.keyCode==13){ po_revdate.focus()}" value="{{ $job->po_no }}" class="form-control" name="po_no" id="" placeholder="P/O No.">
							@elseif(isset($jobs))
								<input type="text" onKeyDown="if(event.keyCode==13){ po_revdate.focus()}" value="{{ $jobs[0]->po_no }}" class="form-control" name="po_no" id="" placeholder="P/O No.">
							@else
								 <input type="text" onKeyDown="if(event.keyCode==13){ po_revdate.focus()}" class="form-control" name="po_no" id="" placeholder="P/O No.">
							@endif
						</div>
						<div class="col-md-3">
							<label for="">Date Rec.P/O</label>
								<div class="ui calendar example2" id="">
									<div class="ui input left icon" style="width: 100%;">
										<i class="calendar icon"></i>
										@if (isset($job))
											<input type="text" onKeyDown="if(event.keyCode==13){ due_po.focus()}" value="{{ $job->po_revdate }}" placeholder="Date" name="po_recdate">
										@elseif(isset($jobs))
											<input type="text" onKeyDown="if(event.keyCode==13){ due_po.focus()}" value="{{ $jobs[0]->po_revdate }}" placeholder="Date" name="po_recdate">
										@else
											<input type="text" onKeyDown="if(event.keyCode==13){ due_po.focus()}" placeholder="Date" name="po_revdate">
										@endif
									</div>
								</div>
						</div>
					 	<div class="col-md-3">
							 <label for="">Due P/O</label>
								<div class="ui calendar example2" id="">
									<div class="ui input left icon" style="width: 100%;">
										<i class="calendar icon"></i>
										@if (isset($job))
											<input type="text" onKeyDown="if(event.keyCode==13){ duedate_po.focus()}" value="{{ $job->due_po }}" placeholder="Due P/O" name="due_po">
										@elseif(isset($jobs))
											<input type="text" onKeyDown="if(event.keyCode==13){ duedate_po.focus()}" value="{{ $jobs[0]->due_po }}" placeholder="Due P/O" name="due_po">
										@else
											<input type="text" onKeyDown="if(event.keyCode==13){ duedate_po.focus()}" placeholder="Due P/O" name="due_po">
										@endif
									</div>
								</div>
							{{-- <label for="">Due P/O</label>
							@if (isset($job))
								<input type="text" onKeyDown="if(event.keyCode==13){ duedate_po.focus()}" value="{{ $job->due_po }}" class="form-control" id="" name="due_po" placeholder="Due P/O">
							@elseif(isset($jobs))
								<input type="text" onKeyDown="if(event.keyCode==13){ duedate_po.focus()}" value="{{ $jobs[0]->due_po }}" class="form-control" id="" name="due_po" placeholder="Due P/O">
							@else
								<input type="text" onKeyDown="if(event.keyCode==13){ duedate_po.focus()}" class="form-control" id="" name="due_po" placeholder="Due P/O">
							@endif --}}
					 	</div>
					 	<div class="col-md-3">
							<label for="">Due Date</label>
								<div class="ui calendar example2" id="">
									<div class="ui input left icon" style="width: 100%;">
										<i class="calendar icon"></i>
										@if (isset($job))
											<input type="text" onKeyDown="if(event.keyCode==13){ qty.focus()}" value="{{ $job->duedate_po }}" placeholder="Date" name="duedate_po">
										@elseif(isset($jobs))
											<input type="text" onKeyDown="if(event.keyCode==13){ qty.focus()}" value="{{ $jobs[0]->duedate_po }}" placeholder="Date" name="duedate_po">
										@else
											<input type="text" onKeyDown="if(event.keyCode==13){ qty.focus()}" placeholder="Date" name="duedate_po">
										@endif
									</div>
								</div>
					 	</div>
					 </div>
				</div>
				{{--///////////////////////////////// part 2 /////////////////////////////////--}}
				<div class="card-body card border-secondary  mb-3 col-md-12">
					<div class="row">
						<div class="col-md-7" style="text-align: right;padding-right: 5%;font-size: 16px;">
							<p>รายละเอียด Part-No </p>
						</div>
						<div class="col-md-5">
							<font class="float-right" color="red" style="font-size: 13px;"><span style="font-size: 30px;">*</span>จำเป็นต้องกรอกเพื่อบันทึก</font>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<label for="">Part No.<font color="red" style="font-size:20px;">*</font></label>
							<br>
							@if (isset($item_no))
								<input type="text" readonly required value="{{ $item_no }}" name="item_no" readonly class="form-control-no" id="item_no" placeholder="Part No./Type" style="width:100%" maxlength="8" >
							@elseif(isset($jobs))
								<input type="text" readonly required value="{{ $jobs[0]->item_fullname }}" name="item_no" readonly class="form-control-no" id="item_no" placeholder="Part No./Type" style="width:100%" maxlength="8" >
							@else
								<input type="text" readonly value="1/1" class="form-control-no" name="item_no" id="item_no" placeholder="Part No./Type" style="width: 100%" maxlength="8" >
							@endif
						</div>
						<div class="col-md-4">
							<label for="">PartStatus<font color="red" style="font-size:20px;">*</font></label>
							<select class="form-control" name="item_status">
							@php
							$status_list = array(
									'1' => 'WorkInProcess',
									'2' => 'Finish',
									'3' => 'Cancel',
								);
							if (isset($jobs)) {
								foreach ($status_list as $key => $value) {
									if ($jobs[0]->item_status == $value) {
										echo '<option selected value='.$value.'>'.$value.'</option>';
									} else {
										echo '<option value='.$value.'>'.$value.'</option>';
									}
								}
							} else {
								foreach ($status_list as $key => $item) {
									if ($key == 1) {
										echo '<option selected value='.$item.'>'.$item.'</option>' ;
									}else{
										echo '<option value='.$item.'>'.$item.'</option>' ;
									}
								}
							}
							@endphp	
							</select>
						</div>
						<div class="col-md-4 ">
							<label for="">Qty<font color="red" style="font-size:20px;">*</font></label>
								<br>
								@if (isset($jobs))
									<input type="text" onKeyDown="if(event.keyCode==13){ draw.focus()}" name="qty" value="{{ $jobs[0]->order_qty }}" class="form-control-no " id="qty" placeholder="Order Qty" style="width: 45%;" >
								@else
									<input type="text" onKeyDown="if(event.keyCode==13){ draw.focus()}" name="qty" class="form-control-no " id="qty" placeholder="Order Qty " style="width: 45%;">
								@endif
								<select class="form-control-no" name="qty_unit" style="width: 45%">
									@php
										$status_order = array(
											'1' => 'Pcs',
											'2' => 'Set',
											'3' => 'EA'
										);
									if (isset($jobs[0]->order_qty)) {
										foreach ($status_order as $key => $value2) {
											if ($jobs[0]->order_qty == $value2) {
												echo '<option selected value='.$value2.'>'.$value2.'</option>';
											} else {
												echo '<option value='.$value2.'>'.$value2.'</option>';
											}
										}
									} else {
										foreach ($status_order as $key => $item2) {
											if ($key == 1) {
												echo '<option selected value='.$item2.'>'.$item2.'</option>' ;
											}else{
												echo '<option value='.$item2.'>'.$item2.'</option>' ;
											}
										}
									}
									@endphp
								</select>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-6" >
								<label for="">Drawing No.</label>
								@if (isset($jobs))
									<input type="text" onKeyDown="if(event.keyCode==13){ rev.focus()}" value="{{ $jobs[0]->drawing_no }}" class="form-control" name="draw" id=draw placeholder="Drawing No.">
								@else
									<input type="text" onKeyDown="if(event.keyCode==13){ rev.focus()}" class="form-control" name="draw" id="draw" placeholder="Drawing No.">
								@endif
						</div>
						<div class="col-md-6 " >
							<label for="">Rev.</label>
							<div class="input-group-append">
								@if (isset($jobs))
									<input type="text" onKeyDown="if(event.keyCode==13){ discrip1.focus()}" value="{{ $jobs[0]->rev }}" class="form-control" name="rev" id="rev" placeholder="Rev." style="width:93%" maxlength="4" >
								@else
									<input type="text" onKeyDown="if(event.keyCode==13){ discrip1.focus()}" class="form-control" name="rev" id="rev" placeholder="Rev." style="width:93%" maxlength="4" >
								@endif
								</div>
						</div>
						{{-- <div class="col-md-4"> --}}
							{{-- <label for="">Qty/Set</label>
								<br>
								@if (isset($jobs))
									<input type="text" value="{{ $jobs[0]->qty_set }}" class="form-control-no" id="" name="qty_set" placeholder="Qty/Set" style="width: 45%">
								@else
									<input type="text" class="form-control-no" id="" name="qty_set" placeholder="Qty/Set" style="width: 45%">
								@endif
								<select class="form-control-no" name="qty_set_unit" style="width: 45%">
									@php
										$status_order = array(
											'1' => 'Pcs',
											'2' => 'Set',
										);
									if (isset($jobs[0]->qty_unit)) {
										foreach ($status_order as $key => $value2) {
											if ($jobs[0]->qty_unit == $value2) {
												echo '<option selected value='.$value2.'>'.$value2.'</option>';
											} else {
												echo '<option value='.$value2.'>'.$value2.'</option>';
											}
										}
									} else {
										echo '<option disabled selected>---- เลือกหน่วย ----</option>';
										foreach ($status_order as $key => $item2) {
											echo '<option value='.$item2.'>'.$item2.'</option>' ;
										}
									}
									@endphp
								</select> --}}
						{{-- </div> --}}
					</div>
					<br>
					<div class="row">
						<div class="col-md-6">
							<label for="">Discription 1</label>
							@if (isset($jobs))
								<input type="text" onKeyDown="if(event.keyCode==13){ discrip2.focus()}" value="{{ $jobs[0]->discirption_1 }}" class="form-control" id="dis1" name="discrip1" placeholder="Discription 1">
							@else
								<input type="text" onKeyDown="if(event.keyCode==13){ discrip2.focus()}" class="form-control" id="dis1" name="discrip1" placeholder="Discription 1">
							@endif
						</div>
						<div class="col-md-6">
							<label for="">Discription 2</label>
							@if (isset($jobs))
								<input type="text" onKeyDown="if(event.keyCode==13){ material.focus()}" value="{{ $jobs[0]->discirption_2 }}" class="form-control" id="dis2" name="discrip2" placeholder="Discription 2" style="width:93%">
							@else
								<input type="text" onKeyDown="if(event.keyCode==13){ material.focus()}" class="form-control" id="dis2" name="discrip2" placeholder="Discription 2" style="width:93%" >
							@endif
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
								<label for="">Meterial</label>
								@if (isset($jobs))
									<input type="text" onKeyDown="if(event.keyCode==13){ hardness.focus()}" value="{{ $jobs[0]->material }}" class="form-control" id="" name="material" placeholder="Meterial">
								@else
									<input type="text" onKeyDown="if(event.keyCode==13){ hardness.focus()}" class="form-control" id="" name="material" placeholder="Meterial">
								@endif
						</div>
						<div class="col-md-3">
								<label for="">Hardness</label>
								@if (isset($jobs))
									<input type="text" onKeyDown="if(event.keyCode==13){ in_no.focus()}" value="{{ $jobs[0]->hardness }}" class="form-control" name="hardness" id="" placeholder="Hardness" maxlength="6" >
								@else
									<input type="text" onKeyDown="if(event.keyCode==13){ in_no.focus()}" class="form-control" name="hardness" id="" placeholder="Hardness" maxlength="6" >
								@endif
						</div>
						<div class="col-md-3">
							{{-- <label for="">Assign By</label>
							@if (isset($name_login))
								<input type="text" required readonly value="{{ $name_login }}" name="assing_by" class="form-control" id="" placeholder="Assign By">
							@elseif(isset($jobs))
								<input type="text" readonly value="{{ $jobs[0]->assign_by }}" name="assing_by" class="form-control" id="" placeholder="Assign By">
							@else
								<input type="text" readonly value="" name="assing_by" class="form-control" id="" placeholder="Assign By">
							@endif --}}
							<label for="">Invoice No.</label>
							@if (isset($jobs))
								<input type="text" onKeyDown="if(event.keyCode==13){ temp.focus()}" value="{{ $jobs[0]->invoice_no }}" name="in_no" class="form-control" id="" placeholder="Invoice No." >
							@else
								<input type="text" onKeyDown="if(event.keyCode==13){ temp.focus()}" name="in_no" class="form-control" id="" placeholder="Invoice No." >
							@endif
						</div>

						<div class="col-md-3" >
								<label for="">Invoice Date</label>
								<div class="input-group" >
									<div class="ui calendar example2" id="">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											@if (isset($jobs))
												<input type="text" onKeyDown="if(event.keyCode==13){ wip_remark.focus()}" value="{{ $jobs[0]->invoice_date }}" name="in_date" placeholder="Date"  >
											@else
												<input type="text" onKeyDown="if(event.keyCode==13){ wip_remark.focus()}" name="in_date" placeholder="Date" >
											@endif
											<div class="input-group-append">
											<button class="fas fa-search" type="button" style="width: 35px;"></button>
											</div>
										</div>
									</div>
								</div>
						</div>

					</div>
					<br>
					<div class="row">
						<div class="col-md-4">
							<label for="">Temp No.</label>
							@if (isset($jobs))
								<input type="text" onKeyDown="if(event.keyCode==13){ in_date.focus()}" value="{{ $jobs[0]->temp_no }}" name="temp" class="form-control" id="" placeholder="Temp No.">
							@else
								<input type="text" onKeyDown="if(event.keyCode==13){ in_date.focus()}" name="temp" class="form-control" id="" placeholder="Temp No.">
							@endif
						</div>
						<div class="col-md-4">
							<label for="">Finish Date</label>
							<div class="input-group mb-3">
								@if (isset($jobs))
									<div class="ui calendar example2" id="">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
												<input type="text" readonly value="{{ $jobs[0]->finish_date }}" name="finish_date" placeholder="Date" style="width: 340px">
											<div class="input-group-append">
											<button class="fas fa-search" type="button" style="width: 35px"></button>
											</div>
										</div>
									</div>
								@else
									{{-- <div class="ui calendar example2" id="example2"> --}}
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											<input type="text" readonly name="finish_date" placeholder="Date" style="width: 340px">
											<div class="input-group-append">
												<button class="fas fa-search" type="button" style="width: 35px"></button>
											</div>
										</div>
									{{-- </div> --}}
								@endif
							</div>
						</div>
							<!-- <div class="col-md-3">
								<label for="">Invoice Date</label>
								<div class="input-group mb-3">
									<div class="ui calendar example2" id="">
										<div class="ui input left icon">
											<i class="calendar icon"></i>
											{{-- @if (isset($jobs))
												<input type="text" onKeyDown="if(event.keyCode==13){ wip_remark.focus()}" value="{{ $jobs[0]->invoice_date }}" name="in_date" placeholder="Date" style="width: 300px">
											@else
												<input type="text" onKeyDown="if(event.keyCode==13){ wip_remark.focus()}" name="in_date" placeholder="Date" style="width: 300px">
											@endif --}}
											<div class="input-group-append">
											<button class="fas fa-search" type="button" style="width: 35px"></button>
											</div>
										</div>
									</div>
								</div>
								{{-- <label for="">Assign Date</label>
								@if (isset($jobs))
									<input type="text" readonly value="{{ $jobs[0]->assign_date }}" name="as_date" class="form-control" id="" placeholder="Assign Date">
								@else
									<input type="text" readonly value="{{ date('M d,Y') }}" name="as_date" class="form-control" id="" placeholder="Assign Date">
								@endif --}}
							</div> -->
					</div>
					<div class="row">
						<div class="col-md-6">
								<label for="">WIP Remark</label>
								@if (isset($jobs))
									<input type="text" onKeyDown="if(event.keyCode==13){ remark.focus()}" value="{{ $jobs[0]->wip_remark }}" name="wip_remark" class="form-control" id="" placeholder="WIP Remark">
								@else
									<input type="text" onKeyDown="if(event.keyCode==13){ remark.focus()}" name="wip_remark" class="form-control" id="" placeholder="WIP Remark">
								@endif
						</div>
						<div class="col-md-6">
								<label for="">Remark</label>
								@if (isset($jobs))
									<input type="text" value="{{ $jobs[0]->remark }}" name="remark" class="form-control" id="" placeholder="Remark">
								@else
									<input type="text" name="remark" class="form-control" id="" placeholder="Remark">
								@endif
						</div>
					</div>
				</div>
				<div class="card-body card border-secondary  mb-3 col-md-12">
					<table class="table table-bordered table-striped" id="table_data112">
					        <thead class="btn-secondary" >
							<tr style="text-align: center;">
								<th style="width:10%">Part No.</th>
								<th style="width:15%">Description</th>
								<th style="width:15%">Status</th>
								<th style="width:10%">Dwq No.</th>
								<th style="width:10%">Rev.</th>
								<th style="width:5%">Material</th>
								<th style="width:5%">Order</th>
								<th style="width:7%">Select</th>
								<th style="width:7%">Delete</th>
							</tr>
					        </thead>
					        <tbody>
							@if (isset($item_show))
								@foreach ($item_show as $value)
								<tr>
									<td style="text-align: center;padding: 15;">{{ $value->item_fullname }}</td>
									<td style="padding: 15;">{{ $value->discirption_1 }}</td>
									<td style="padding: 15;">{{ $value->item_status }}</td>
									<td style="padding: 15;">{{ $value->drawing_no }}</td>
									<td style="padding: 15;">{{ $value->rev }}</td>
									<td style="padding: 15;">{{ $value->material }}</td>
									<td style="padding: 15;">{{ $value->order_qty }}</td>
									<td style="text-align: center;">{{ Html::link('search/show/'.base64_encode($value->job_no.'@'.$value->item_fullname), 'Select', array('class'=> 'btn btn-secondary')) }}</td>
									<td style="text-align: center;">{{ Html::link('joborder/delete_item/'.base64_encode($value->job_no.'@'.$value->item_fullname), 'Delete', array('class'=> 'btn btn-secondary')) }}</td>
								</tr>
								@endforeach
							@endif
					    </tbody>
					</table>
				</div>
				<div class="card-body card border-secondary  mb-3 col-md-12">
					<div class="row">
						<div class="col-md-6">
							<label for="">Record:</label>
							@if (isset($job))
								<input type="text" disabled value="{{ $job->record_by }}" class="form-control-no" id="" placeholder="Record" style="width: 40%">
								<input type="text" disabled value="{{ $job->record_date }}" class="form-control-no" id="" placeholder="Record" style="width: 40%">
							@elseif(isset($jobs))
								<input type="text" disabled value="{{ $jobs[0]->record_by }}" class="form-control-no" id="" placeholder="Record" style="width: 40%">
								<input type="text" disabled value="{{ $jobs[0]->record_date }}" class="form-control-no" id="" placeholder="Record" style="width: 40%">
							@else
								<input type="text" disabled value="" class="form-control-no" id="" placeholder="Record" style="width: 40%">
								<input type="text" disabled value="" class="form-control-no" id="" placeholder="Record" style="width: 40%">
							@endif
							
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<br>
							<label for="">Update:</label>
							@if (isset($job))
								<input type="text" disabled value="{{ $job->update_by }}" class="form-control-no" id="" placeholder="Update" style="width: 40%">
								<input type="text" disabled value="{{ $job->update_date }}" class="form-control-no" id="" placeholder="Update" style="width: 40%">
							@elseif(isset($jobs))
								<input type="text" disabled value="{{ $jobs[0]->update_by }}" class="form-control-no" id="" placeholder="Record" style="width: 40%">
								<input type="text" disabled value="{{ $jobs[0]->update_date }}" class="form-control-no" id="" placeholder="Record" style="width: 40%">
							@else
								<input type="text" disabled value="" class="form-control-no" id="" placeholder="Update" style="width: 40%">
								<input type="text" disabled value="" class="form-control-no" id="" placeholder="Update" style="width: 40%">
							@endif
							
						</div>
						<div class="col-md-6 ">
								<br>
								<button type="submit" class="btn btn-secondary float-right">Save/Update</button>
						</div>
					</div>
				</div>
				<br>
			</div>
			{{ Form::close() }}
		{{-- card body --}}
		</div> 
</div>
</body>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document" style="max-width:none;width:90%;">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Search Item</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-5">
						@php
							$status_modal_1 =array(
								'1' => 'JobNo',
								'2' => 'P/ONo',
								'3' => 'InvoiceNo',
								'4' => 'TempNo'
							);
						@endphp
						<label for="">ค้นหาจาก</label>
						<br>
						<select class="form-control-no search_data" style="width:50%" id="search_type">
						@foreach ($status_modal_1 as $key => $status_modal_1)
							<option value="{{ $key }}">{{ $status_modal_1 }}</option>
						@endforeach
						</select>
						<select class="form-control-no search_data" style="width:30%" id="search_year">
						@for ($i = 2018; $i < 2048; $i++)
							<option value="{{ $i }}">{{ $i }}</option>
						@endfor
						</select>
					</div>
					<div class="col-md-2" style="padding-right: 0px;">
						<label for="">Status</label>
						<br>
						<select class="form-control-no search_data" style="width:100%;" id="search_status">
						@php
							$status_modal =array(
								'0' => 'All',
								'1' => 'WorkinProcess',
								'2' => 'Finish',
								'3' => 'Cancle'
							);
						@endphp
						@foreach ($status_modal as $key => $status_modal)
							<option value="{{ $key }}">{{ $status_modal }}</option>
						@endforeach
						</select>
					</div>
					<div class="col-md-3" style="padding-left: 3px;">
						{{-- <label for="">คำที่ต้องการค้นหา</label> --}}
						{{-- <input type="text" class="form-control-no search_data" style="width:100%;" id="search_text"> --}}
					</div>
					<div class="col-md-2">
						{{-- <div class="float-right">
							<label for="">Status</label>
							<br>
							<input readonly type="text"  class="form-control-no " style="width:100%" id="total">
						</div> --}}
					</div>
				</div>
					<br>
				{{-- <div class="row"> --}}
					{{-- <div class="col-md-12"> --}}
						<table class="table table-bordered table-striped" id="table_data3">
							<thead class="btn-secondary">
								<tr style="width:100%">
									<td style="width:10%">Action</td>
									<td style="width:10%">Job No</td>
									<td style="width:10%">Status</td>
									<td style="width:10%">Job date</td>
									<td style="width:10%">Due date</td>
									<td style="width:10%">P/O No</td>
									<td style="width:30%">Customer</td>
									<td style="width:10%">Part No</td>
									<td style="width:10%">Part Status</td>
									<td style="width:10%">Qty Order</td>
									<td style="width:10%">Temp No</td>
									<td style="width:10%">Invoice No</td>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					{{-- </div> --}}
				{{-- </div> --}}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				{{-- <button type="button" class="btn btn-primary">Search</button> --}}
			</div>
		</div>
	</div>
</div>
@if (isset($item_show))
<div class="modal fade" id="Modal_print" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document" style="max-width:none;width:30%;">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Search Item</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
					{{ Form::open(['url'=>'report/jobassign','id'=>'modal_print']) }}
						<h2 style="margin-top: -3px;">Print Job Assignment</h2>
						<div class="input-group" style="padding-bottom: 7px;">
							<div class="input-group-prepend">
								<div class="input-group-text">
									<input type="radio" name="print" value="1" aria-label="Radio button for following text input">
								</div>
							</div>
							<input type="text" readonly class="form-control-no" value="ปริ้นทั้งหมด" aria-label="Text input with radio button">
						</div>
						<div class="input-group" style="padding-bottom: 7px;">
							<div class="input-group-prepend">
								<div class="input-group-text">
									<input type="radio" name="print" value="2" aria-label="Radio button for following text input">
								</div>
							</div>
							<input type="text" readonly class="form-control-no" value="เลือกปริ้นจาก Part No" aria-label="Text input with radio button">
						</div>
						<select id="chk_item" name="item"  class="form-control-no search_data" id="search_year">
						@foreach ($item_show as $print1)
							<option value="{{ $print1->job_no.'@'.$print1->item_fullname }}">{{ 'JobNo : '.$print1->job_no.' , Part No : '.$print1->item_fullname }}</option>
						@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-secondary">Print</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>
@endif
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
        $(function() {
			$("#cus_code").change(function(){
				var id_cus = $('#cus_code').val();
				$.ajax({
					type: "GET",
					url: 'serach_cus/'+id_cus,
					dataType: 'json',
					success: function(result) {
						if (result != '') {
							// console.log(result);
							var cus_no = result.customer[0].cus_no;
							var id_cus = result.customer[0].id_cus;
							var cus_name = result.customer[0].cus_name;
							var job_no = result.jobno;
							$("#cus_id").val(cus_no);
							$("#cus_code").val(id_cus);
							$("#cust_name").val(cus_name);
							$("#job_no").val(job_no);
						}else{
							swal({
								title: "ไม่พบข้อมูล",
								text: "กรุณากรอกรหัสลูกค้าใหม่",
								icon: "warning",
								dangerMode: true,
							})
						}
					}
				})
			})
		})
		$(function() {
			$("#rev").change(function(){
				var draw = $('#draw').val();
				var rev = $('#rev').val();
				$.ajax({
					type: "GET",
					url: "{!! url('serach_dis/"+draw+"@"+rev+"') !!}",
					dataType: 'json',
					success: function(result) {
						// console.log(result);
						if (result.total != 0) {
							var dis1 = result.items[0].dis1;
							var dis2 = result.items[0].dis2;
							$("#dis1").val(dis1);
							$("#dis2").val(dis2);
						}else{
							swal({
								title: "ไม่พบข้อมูล Discription",
								text: "ตรวจสอบข้อมูลให้ถูกต้องทั้ง DrawingNo. และ Rev.",
								icon: "error",
								dangerMode: true,
							})
						}
					}
				})
			})
		})
	$(window).load(function(){
		check_pn = $("#item_no").val();
		if (check_pn == "1/1") {
			// $("#qty").attr("required","required");
				$("#job_form").on('submit',(function() {
					var v_qty = $('#qty').val();
					// alert("Test");
					if (v_qty=="") {
						$('#qty').addClass('hd_danger')
						$('#qty').focus()
						swal({
						title: "กรุณากรอกข้อมูล Order Qty",
						text: "จำเป็นต้องกรอกข้อมูล Order Qty ในครั้งแรกที่เปิด Job",
						icon: "warning",
						dangerMode: true,
						})

						// alert("No");
						return false;
					}
			}));
		}

		$('#table_data112').DataTable( { } );
		var oTable = $('#table_data3').DataTable({
			processing: true,
			serverSide: true, 
			searching: true, 
			"oLanguage": { "sSearch": "คำที่ต้องการค้นหา :" },
			lengthChange: true,
			ajax:{
				url : "{{url('serach_show')}}",
				data: function (d) {
					d.type = $('#search_type').val();
					d.date = $('#search_year').val();
					d.status = $('#search_status').val();
					d.text = $('#search_text').val();
            	}
			},
			"order": [[ 3, "desc" ]],
			columns: [
				{ data: 'job_no', name: 'job_no' },
				{ data: 'job_no', name: 'job_no' },
				{ data: 'job_status2', name: 'job_status2' },
				{ data: 'job_date', name: 'job_date' },
				{ data: 'due_po', name: 'due_po' },
				{ data: 'po_no', name: 'po_no'},
				{ data: 'cust_name', name: 'cust_name'},
				{ data: 'item_fullname', name: 'item_fullname' },
				{ data: 'item_status', name: 'item_status'},
				{ data: 'order_qty', name: 'order_qty'},
				{ data: 'temp_no', name: 'temp_no'},
				{ data: 'invoice_no', name: 'invoice_no'},
			], 
			// "scrollX": true,
		rowCallback: function(row,data,index ){
			$('td:eq(0)', row).html(
				'<a href="../../../happyday/search/show/'+data['code']+'" class="btn btn-secondary">Select</a>'
			);
		}
		});
		$('.search_data').change(function(e){
			oTable.draw();
			e.preventDefault();
		});
	});
</script>
<script type="text/javascript">
	$(function() {
		    // $(".css_data_item").click(function(){  // เมื่อคลิก checkbox  ใดๆ  
			// 	if($(this).prop("checked")==true){ // ตรวจสอบ property  การ ของ   
			// 		var indexObj=$(this).index(".css_data_item"); //   
			// 		$(".css_data_item").not(":eq("+indexObj+")").prop( "checked", false ); // ยกเลิกการคลิก รายการอื่น  
			// 	}  
			// }); 
		$("#delete").click(function(){
			var id_del = $('#delete').val();
			swal({
				title: "ต้องการลบข้อมูลหรือไม่ ?",
				text: "การลบข้อมูล Job จะทำให้ข้อมูลทั้งหมดของ Job ลบไปอย่างถาวร",
				icon: "warning",
				buttons: true,
				dangerMode: true,
				})
				.then((willDelete) => {
				if (willDelete) {
					// function(){
					$.ajax({
						type:"GET",
						url:"{!! url('joborder/delete/"+id_del+"') !!}",
						dataType: 'json',
						error: function(){
							swal("ลบข้อมูลไม่สำเร็จ", {
								icon: "error",
							})
						},
						success: function(result) {
							swal("ลบข้อมูลเสร็จสิ้น", {
								icon: "success",
							})
							.then((willDelete) => {
								window.location.href = "{{URL::to('joborder')}}"
							});
						},
					});
				} else {
					swal("ยกเลิกการลบข้อมูล",{
					icon: "error",
					});
				}
			});
		})
	});
</script>