@include('layouts.menu')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<style>
    table,
    td,
    th {
        padding: 5px;
        padding-left: 10px;
        padding-right: 10px;
        border-collapse: collapse;
    }
    .container {
        max-width: 1300px;
    }
</style>
<body>
    <br>
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-md-12" style="padding-left: 45%;"><h3>Work in Process</h3></div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label for="">Job date:</label>
                </div>
                <div class="col-md-8" style="padding-left: 100px;">
                    <h3>Happy Day Engineering Co.,Ltd.</h3>
                </div>
            </div>
            <br>
            <div>
                <table style="width:100%" border="1px">
                    <tr style="text-align:center;">
                        <th style="width:7%">Job No.</th>
                        <th style="width:3%">Item</th>
                        <th style="width:14%">Job Open Date</th>
                        <th style="width:13%">Drawing No.</th>
                        <th style="width:3%">Rev.</th>
                        <th style="width:18%">Description</th>
                        <th style="width:3%">Qty.</th>
                        <th style="width:3%">U/M</th>
                        <th style="width:8%">P/O Number</th>
                        <th style="width:10%">Due Original</th>
                        <th style="width:18%">Remark</th>
                    </tr>
                @foreach ($jobs as $value)
                    <tr>
                        <td colspan="12">
                            <label for="" style="padding-top:5px;">Company : {{ $jobs[0]->cust_name }}</label>
                        </td>
                    </tr>
                    <tr style="text-align:center;">
                        <td style="width:7%">{{ $value->job_no }}</td>
                        <td style="width:3%">{{ $value->item_fullname }}</td>
                        <td style="width:14%">{{ $value->job_date }}</td>
                        <td style="width:13%">{{ $value->drawing_no }}</td>
                        <td style="width:3%">{{ $value->rev }}</td>
                        <td style="width:18%">{{ $value->discirption_1 }}</td>
                        <td style="width:3%">{{ $value->order_qty }}</td>
                        <td style="width:3%">{{ $value->order_unit }}</td>
                        <td style="width:8%">{{ $value->po_no }}</td>
                        <td style="width:10%">{{ $value->duedate_po }}</td>
                        <td style="width:18%">{{ $value->remark }}</td>
                    </tr>
                @endforeach
                    {{-- @php
                        $count = 40-count($jobs);
                    @endphp
                    @for ($i = 0; $i <= $count; $i++)
                        <tr style="text-align:center;">
                            <td style="width:7%;padding:10px"></td>
                            <td style="width:3%"></td>
                            <td style="width:14%"></td>
                            <td style="width:13%"></td>
                            <td style="width:3%"></td>
                            <td style="width:18%"></td>
                            <td style="width:3%"></td>
                            <td style="width:3%"></td>
                            <td style="width:8%"></td>
                            <td style="width:10%"></td>
                            <td style="width:18%"></td>
                        </tr>
                    @endfor --}}
                </table>
                <br>
            </div>
        </div>
    </div>
</body>
</html>