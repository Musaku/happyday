@include('layouts.menu')
<head>

</head>
<body>
    <div class="container">
        <br>
        <div><h2>ระบบเพิ่มสินค้า</h2></div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card">
            <div class="card-header bg-secondary text-white">
                รายการสินค้า
                    <div class="float-right">
                        <a class="" href="{{url('product/create')}}" style="padding-right:5px">
                            <button type="button" class="btn btn-secondary btn-sm">เพิ่มข้อมูลสินค้า</button>
                        </a>
                        <a class="" href="{{url('product/show')}}" style="">
                            <button type="button" class="btn btn-secondary btn-sm">จัดการข้อมูลสินค้า</button>
                        </a>
                    </div>
                {{-- <div class="row">
                    <div class="col-md-4">
                        @if (isset($product))
                        <p style="padding-top:8px">Edit Form</p>
                        @else
                        <p style="padding-top:8px">Create Form</p>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <div class="float-right">
                            <a class="" href="{{url('product/create')}}" style="padding-right:5px">
                                <button type="button" class="btn btn-primary">เพิ่มข้อมูลสินค้า</button>
                            </a>
                            <a class="" href="{{url('product/show')}}" style="">
                                <button type="button" class="btn btn-primary">จัดการข้อมูลสินค้า</button>
                            </a>
                        </div>
                    </div>
                </div> --}}
            </div>
            <div class="card-body">
                <div class="card-body card border-secondary mb-3">
                    @if (isset($product))
                        {{Form::open(['url'=>['product/update',$product->id],'enctype'=>'multipart/form-data'])}}
                    @else
                        {{Form::open(['url'=>'product/store','enctype'=>'multipart/form-data'])}}
                    @endif
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">รหัสลูกค้า</label>
                            @if (isset($product))
                                {{Form::text('code',$product->cust_no,['class' => 'form-control','placeholder' => 'รหัสลูกค้า','required'])}}
                            @else
                                {{Form::text('code','',['class' => 'form-control','placeholder' => 'รหัสลูกค้า','required'])}}
                            @endif
                        </div>
                        <div class="col-md-4">
                            <label for="">Drawing No</label>
                            @if (isset($product))
                                 {{Form::text('drw',$product->drwing,['class' => 'form-control','placeholder' => 'Drawing No','required'])}}
                            @else
                                {{Form::text('drw','',['class' => 'form-control','placeholder' => 'Drawing No','required'])}}
                            @endif
                        </div>
                        <div class="col-md-4">
                            <label for="">Rev</label>
                            @if (isset($product))
                                {{Form::text('rev',$product->rev,['class' => 'form-control','placeholder' => 'Rev','required'])}}
                            @else
                                {{Form::text('rev','',['class' => 'form-control','placeholder' => 'Rev','required'])}}
                            @endif
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">Drawing & Rev</label>
                            @if (isset($product))
                                {{Form::text('drw_rev',$product->drw_rev,['class' => 'form-control','placeholder' => 'Drawing & Rev','required'])}}
                            @else
                                {{Form::text('drw_rev','',['class' => 'form-control','placeholder' => 'Drawing & Rev','required'])}}
                            @endif
                        </div>
                        <div class="col-md-4">
                            <label for="">Discription 1</label>
                            @if (isset($product))
                                {{Form::text('dis1',$product->dis1,['class' => 'form-control','placeholder' => 'Discription 1','required'])}}
                            @else
                                {{Form::text('dis1','',['class' => 'form-control','placeholder' => 'Discription 1','required'])}}
                            @endif
                        </div>
                        <div class="col-md-4">
                            <label for="">Discription 2</label>
                            @if (isset($product))
                                {{Form::text('dis2',$product->dis2,['class' => 'form-control','placeholder' => 'Discription 2'])}}
                            @else
                                {{Form::text('dis2','',['class' => 'form-control','placeholder' => 'Discription 2'])}}
                            @endif
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="ui buttons">
                            {{Form::submit('Save',['class' => 'ui primary  button'])}}
                            <div class="or"></div>
                            </div>
                            {{Form::reset('Clear',['class' => 'ui  button'])}}
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
</body>