@include('layouts.menu')
<head>

</head>
<body>
     <br>
    <div class="container">
        <div><h2>ระบบจัดการพนักงาน</h2></div>
        <div class="card">
            <div class="card-header bg-secondary text-white">
                    รายการสินค้า
                    <div class="float-right">
                        <a class="" href="{{url('product/excel')}}" style="">
                            <button type="button" class="btn btn-secondary btn-sm">เพิ่มข้อมูลจาก Excel</button>
                        </a>
                        <a class="" href="{{url('product/create')}}" style="padding-right:5px">
                            <button type="button" class="btn btn-secondary btn-sm">เพิ่มข้อมูลสินค้า</button>
                        </a>
                        <a class="" href="{{url('product/show')}}" style="">
                            <button type="button" class="btn btn-secondary btn-sm">จัดการข้อมูลสินค้า</button>
                        </a>
                    </div>
            </div>
            <div class="card-body">
                    <div class="row" style="padding-left: 8px;">
                        <table class="table table-bordered table-striped" id="table_data4">
                            <thead class="btn-secondary" >
                            <tr width=100% style="text-align:center">
                                <th width="5%" style="text-align:center;">No</th>
                                <th width="10%" style="text-align:center;">รหัสลูกค้า</th>
                                <th width="15%" style="text-align:center;">Drawing No</th>
                                <th width="5%" style="text-align:center;">Rev</th>
                                <th width="15%" style="text-align:center;">Drawing&Rev</th>
                                <th width="15%" style="text-align:center;">Discription1</th>
                                <th width="15%" style="text-align:center;">Discription2</th>
                                <th width="11%" style="text-align:center;">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                           
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>
</body>
<script>
    $(window).load(function(){
		var oTable = $('#table_data4').DataTable({
			processing: true,
			serverSide: true, 
			searching: true, 
			lengthChange: true,
			ajax:{
				url : "{{url('list_product')}}",
			},
			columns: [ 
				{ data: 'no', name: 'no' },
				{ data: 'cust_no', name: 'cust_no' },
				{ data: 'drwing', name: 'drwing' },
				{ data: 'rev', name: 'rev' },
				{ data: 'drw_rev', name: 'drw_rev' },
				{ data: 'dis1', name: 'dis1'},
				{ data: 'dis2', name: 'dis2'},
                { data: 'dis2', name: 'dis2'},
			], 
		rowCallback: function(row,data,index ){
			$('td:eq(7)', row).html(
				'<a href="../../../happyday/product/edit/'+data['id']+'" class="btn btn-secondary";">Edit</a>'+
                '&nbsp;'+
                '<a href="../../../happyday/product/destroy/'+data['id']+'" class="btn btn-secondary" ; ">Delete</a>'
			);
		}
		});
	});
</script>