@include('layouts.menu')
<head>

</head>
<body>
    <?php if (isset($com)) {  
        if ($com == 1) { ?>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript">
        swal({
                title: "Upload Excel",
                text: "อัพโหลดไฟลเสร็จสิ้น",
                icon: "success",
                successMode: true,
            })
    </script>
    <?php } } ?>
    <br>
    <div class="container">
        <div>
            <h2>ระบบจัดการพนักงาน</h2>
        </div>
        <div class="card">
            <div class="card-header bg-secondary text-white">
                Import Excel
                <div class="float-right">
                    <a class="" href="{{url('product/excel')}}" style="">
                            <button type="button" class="btn btn-secondary btn-sm">เพิ่มข้อมูลจาก Excel</button>
                        </a>
                    <a class="" href="{{url('product/create')}}" style="padding-right:5px">
                            <button type="button" class="btn btn-secondary btn-sm">เพิ่มข้อมูลสินค้า</button>
                        </a>
                    <a class="" href="{{url('product/show')}}" style="">
                            <button type="button" class="btn btn-secondary btn-sm">จัดการข้อมูลสินค้า</button>
                        </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                            {{Form::open(['url'=>['import_excel'],'enctype'=>'multipart/form-data'])}}
                                <h3>เพิ่มข้อมูลด้วย Excel</h3>
                                <input type="file" name="import_file" id="input-file-now" class="dropify" data-default-file="" required />
                                <br>
                                <button class="btn btn-secondary">Import File</button> 
                            {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<footer class="main-footer">
        <script>
            $(document).ready(function(){
                // Basic
                $('.dropify').dropify();
                // Used events
                var drEvent = $('.dropify-event').dropify();
                drEvent.on('dropify.beforeClear', function(event, element){
                    return confirm("Do you really want to delete \"" + element.filename + "\" ?");
                });
                drEvent.on('dropify.afterClear', function(event, element){
                    alert('File deleted');
                });
            });
        </script>
  </footer>
