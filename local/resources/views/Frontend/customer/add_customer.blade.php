@include('layouts.menu')
<body>
     <div class="container">
         <br>
            <div class="row">
                <div class="col-md-4">
                        <h2>ระบบเพิ่มลูกค้า</h2>
                </div>
            </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card">
            <div class="card-header bg-secondary text-white">
                @if (isset($customer))
                    Edit Form
                @else
                    Create Form
                @endif
                <div class="float-right">
                    <a class="" href="{{url('customer/list')}}" style="padding-right:5px">
                        <button type="button" class="btn btn-secondary btn-sm">จัดการข้อมูลลุกค้า</button>
                    </a>
                    <a class="" href="{{url('customer/add')}}" style="">
                        <button type="button" class="btn btn-secondary btn-sm">เพิ่มข้อมูลลูกค้า</button>
                    </a>
                </div>
                {{-- <div class="row">
                    <div class="col-md-3">
                        @if (isset($customer))
                            <p style="padding-top: 8px;">Edit Form</p>
                        @else
                            <p style="padding-top: 8px;">Create Form</p>
                        @endif
                    </div>
                    <div class="col-md-9">
                    <div class="float-right">
                        <a class="" href="{{url('customer/list')}}" style="padding-right:5px">
                            <button type="button" class="btn btn-primary">จัดการข้อมูลลุกค้า</button>
                        </a>
                        <a class="" href="{{url('customer/add')}}" style="">
                            <button type="button" class="btn btn-primary">เพิ่มข้อมูลลูกค้า</button>
                        </a>
                    </div>
                </div>
                </div> --}}
            </div>
            <div class="card-body">
                <div class="card-body card border-secondary mb-3">
                    @if (isset($customer))
                        {{Form::open(['url'=>['customer/update',$customer->id],'enctype'=>'multipart/form-data'])}}
                    @else
                        {{Form::open(['url'=>'customer/store','enctype'=>'multipart/form-data'])}}
                    @endif
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">รหัสลูกค้า</label>
                            @if (isset($customer))
                                 {{Form::text('code',$customer->id_cus,['class' => 'form-control','placeholder' => 'รหัสลูกค้า','required'])}}
                            @else
                                {{Form::text('code','',['class' => 'form-control','placeholder' => 'รหัสลูกค้า','required'])}}
                            @endif
                        </div>
                        <div class="col-md-4">
                            <label for="">ลำดับลูกค้า</label>
                            @if (isset($customer))
                                {{Form::text('Cus_ID',$customer->cus_no,['class' => 'form-control','placeholder' => 'ลำดับลูกค้า','required'])}}
                            @else
                                {{Form::text('Cus_ID','',['class' => 'form-control','placeholder' => 'ลำดับลูกค้า','required'])}}
                            @endif
                        </div>
                        <div class="col-md-4">
                            <label for="">ชื่อลูกค้า</label>
                            @if (isset($customer))
                                {{Form::text('name',$customer->cus_name,['class' => 'form-control','placeholder' => 'ชื่อ นามสกุล','required'])}}
                            @else
                                {{Form::text('name','',['class' => 'form-control','placeholder' => 'ชื่อลูกค้า','required'])}}
                            @endif
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">Tel</label>
                            @if (isset($customer))
                                {{Form::text('Tel',$customer->tel,['class' => 'form-control','placeholder' => 'เบอร์โทรศัพท์','required'])}}
                            @else
                                {{Form::text('Tel','',['class' => 'form-control','placeholder' => 'เบอร์โทรศัพท์','required'])}}
                            @endif
                        </div>
                        <div class="col-md-4">
                            <label for="">Fax</label>
                            @if (isset($customer))
                                 {{Form::text('Fax',$customer->fax,['class' => 'form-control','placeholder' => 'Fax','required'])}}
                            @else
                                {{Form::text('Fax','',['class' => 'form-control','placeholder' => 'Fax','required'])}}
                            @endif
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-8">
                            <label for="">ที่อยู่ลูกค้า</label>
                            @if (isset($customer))
                                {{Form::textarea('address',$customer->cus_address,['class' => 'form-control','placeholder' => 'ที่อยู่ลูกค้า','rows'=>'7' ,'cols'=>'50'])}}
                            @else
                                {{Form::textarea('address','',['class' => 'form-control','placeholder' => 'ที่อยู่ลูกค้า','rows'=>'7' ,'cols'=>'50'])}}
                            @endif
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="ui buttons">
                            {{Form::submit('Save',['class' => 'ui primary  button'])}}
                            <div class="or"></div>
                            </div>
                            {{Form::reset('Clear',['class' => 'ui  button'])}}
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
</body>