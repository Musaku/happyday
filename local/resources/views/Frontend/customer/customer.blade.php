@include('layouts.menu')
<!DOCTYPE html>
<html lang="en">
<head>
</head>
<body>
    <br>
    <div class="container">
        <div>
            <h2>ระบบจัดการข้อมูลลูกค้า</h2>
        </div>
        <div class="card">
            <div class="card-header bg-secondary text-white">
                รายการลูกค้า
                <div class="float-right">
                    <a class="" href="{{url('customer/list')}}" style="padding-right:5px">
                        <button type="button" class="btn btn-secondary btn-sm">จัดการข้อมูลลุกค้า</button>
                    </a>
                    <a class="" href="{{url('customer/add')}}" style="">
                        <button type="button" class="btn btn-secondary btn-sm">เพิ่มข้อมูลลูกค้า</button>
                    </a>
                </div>
                {{-- <div class="row">
                    <div class="col-md-2">
                        <p style="padding-top: 8px;">รายการลูกค้า</p>
                    </div>
                    <div class="col-md-10">
                        <div class="float-right">
                            <a class="" href="{{url('customer/list')}}" style="padding-right:5px">
                                <button type="button" class="btn btn-primary">จัดการข้อมูลลุกค้า</button>
                            </a>
                            <a class="" href="{{url('customer/add')}}" style="">
                                <button type="button" class="btn btn-primary">เพิ่มข้อมูลลูกค้า</button>
                            </a>
                        </div>
                    </div>
                </div> --}}
            </div>
            <div class="card-body">
                {{-- <div class="card-body card border-secondary mb-3"> --}}
                    <div class="row" style="padding-left: 8px;">
                        <table class="table table-bordered table-striped" id="table_data">
                            <thead class="btn-secondary" >
                            <tr>
                                <th width="5%">No</th>
                                <th width="10%">ID CUT</th>
                                <th width="14%">Customer NO</th>
                                <th width="20%">Customer Name</th>
                                <th width="10%">Tel</th>
                                <th width="10%">Fax</th>
                                <th width="20%" aling="center">Action</th>
                            </tr>
                            </thead>
                            @php
                            $i =1;
                            @endphp
                            <tbody>
                            @foreach ($customer as $data)
                                @if (isset($data))
                                <tr>
                                    <td align="center">{{ $i}}</td>
                                    <td>{{ $data['id_cus']}}</td>
                                    <td>{{ $data['cus_no']}}</td>
                                    <td>{{ $data['cus_name']}}</td>
                                    <td>{{ $data['tel']}}</td>
                                    <td>{{ $data['fax']}}</td>
                                    <td align="center">
                                    {{ Form::open(['url' => ['customer/destroy',$data['id']] ]) }}
                                        {{ Html::link('customer/view/'.$data['id'], 'View', array('class'=> 'btn btn-secondary')) }}
                                        {{ Html::link('customer/edit/'.$data['id'], 'Edit', array('class'=> 'btn btn-secondary')) }}
                                        {{ Form::submit('Delete',array('class' => 'btn btn-secondary')) }}
                                    {{ Form::close() }}
                                    </td>
                                </tr>
                                @else
                                <td colspan="6">no data</td>
                                @endif
                                @php
                                $i++;
                                @endphp
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                {{-- </div> --}}
            </div>
        </div>
    </div>
</body>
</html>