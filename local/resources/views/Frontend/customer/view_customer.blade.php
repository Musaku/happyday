@include('layouts.menu')
<head>
    <style>
        .light-grey {
            background-color: #f1f1f1;
        }

        table,
        td,
        th {
            border: 2px solid #adadad;
        }

        .ddc {
            padding-left: 60px;
            position: relative;
            width: 100%;
            min-height: 1px;
            padding-right: 15px;
            position: relative;
            left: 50%;
        }
    </style>
</head>
<body>
    <div class="container">
        <br>
        <div class="card">
            <div class="card-header bg-secondary text-white">
                รายละเอียดลูกค้า
                <div class="float-right">
                    <a class="" href="{{url('customer/list')}}" style="padding-right:5px">
                        <button type="button" class="btn btn-secondary btn-sm">จัดการข้อมูลลุกค้า</button>
                    </a>
                    <a class="" href="{{url('customer/add')}}" style="">
                        <button type="button" class="btn btn-secondary btn-sm">เพิ่มข้อมูลลูกค้า</button>
                    </a>
                </div>
                {{-- <div class="row">
                    <div class="col-md-3">
                        <p style="padding-top:8px">รายละเอียดลูกค้า</p>
                    </div>
                    <div class="col-md-9">
                        <div class="float-right">
                            <a class="" href="{{url('customer/list')}}" style="padding-right:5px">
                                <button type="button" class="btn btn-primary">จัดการข้อมูลลุกค้า</button>
                            </a>
                            <a class="" href="{{url('customer/add')}}" style="">
                                <button type="button" class="btn btn-primary">เพิ่มข้อมูลลูกค้า</button>
                            </a>
                        </div>
                    </div>
                </div> --}}
                
            </div>
            <div class="card-body">
                {{--
                <div class="row cen">
                    <div class="col-md-3 card-body card border-secondary mb-3">
                        <p>ID Customer</p>
                    </div>
                    <div class="col-md-3 card-body card border-secondary mb-3 ">
                        <p>{{ $customer->id_cus }}</p>
                    </div>
                </div> --}}
                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="col-12 col-md-12 ddc">
                            <h4>ข้อมูลเพิ่มเติม</h4>
                            <table class="table">
                                <tr>
                                    <td class="light-grey" width="40%">ID Customer</td>
                                    <td width="60%">{{ $customer->id_cus }}</td>
                                </tr>
                                <tr>
                                    <td class="light-grey">Customer No</td>
                                    <td>{{ $customer->cus_no }}</td>
                                </tr>
                                <tr>
                                    <td class="light-grey">Customer Name</td>
                                    <td>{{ $customer->cus_name }}</td>
                                </tr>
                                <tr>
                                    <td class="light-grey">Tel</td>
                                    <td>{{ $customer->tel }}</td>
                                </tr>
                                <tr>
                                    <td class="light-grey">Fax</td>
                                    <td>{{ $customer->fax }}</td>
                                </tr>
                                <tr>
                                    <td class="light-grey">Customer Address</td>
                                    <td style="max-width:100px">{{ $customer->cus_address }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>