@include('layouts.menu')
<head>
    <style>
    .form-control-no{
		padding: .375rem .75rem;
		font-size: 1rem;
		line-height: 1.5;
		color: #495057;
		background-color: #fff;
		background-clip: padding-box;
		border: 1px solid #ced4da;
		border-radius: .25rem;
		transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
	}
    table, td, th{
            padding: 5px; 
            padding-left: 10px;
            padding-right: 10px;
            border-collapse: collapse;
        }
    </style>
</head>
<br>
<body>
    <div class="container">
        <div class="content">
            <div class="row" style="padding-bottom:10px">
                <div class="col-md-12">
                    <h2 style="text-align:center;">Summary Received Purchase Order Report</h2>
                </div>
            </div>
            <div class="row">
                <table border="1px" style="width:100%;text-align:center;border-color: black;" >
                    <tr style="background-color: #0099cc;">
                        <th style="width:5%">Item</th>
                        <th style="width:20%">Date Received</th>
                        <th style="width:45%">Customer</th>
                        <th style="width:30%">P/O NO.</th>
                    </tr>
                    @for ($i = 1; $i <= 60; $i++)
                        <tr>
                            <td>{{ $i }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endfor
                </table>
            </div>
            <br>
        </div>
    </div>
</body>
