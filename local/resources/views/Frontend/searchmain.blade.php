@include('layouts.menu')
<br>
<style>
    div.dataTables_wrapper {
        width: 100%;
    }
</style>
<body>
<div class="container">
    <div class="content">
        <div class="card">
            <div class="card-header bg-secondary text-white">
                Search Order
            </div>
            <div class="card-body">
                <div class="card-body card border-secondary mb-3">
                    {{Form::open(['url'=>'search/main','enctype'=>'multipart/form-data'])}}
                    <div class="row">
                        <div class="col-md-3">
                            {{ Form::label('label','Customer Name')}}
                            {{Form::text('custname','',['class' => 'form-control','placeholder' => 'Customer Name'])}}
                        </div>
                        <div class="col-md-3">
                            {{ Form::label('label','P/O Number')}}
                            {{Form::text('ponumber','',['class' => 'form-control','placeholder' => 'P/O Name'])}}
                        </div>
                        <div class="col-md-3 ">
                            {{ Form::label('label','Job No')}}
                            {{Form::text('jobno','',['class' => 'form-control','placeholder' => 'Job No'])}}
                                
                        </div>
                        <div class="col-md-3">
                            {{ Form::label('label','Item')}}
                            {{Form::text('item','',['class' => 'form-control','placeholder' => 'Item'])}}
                        </div>
                    </div>
                    <br>
                    <div class="row">
                            <div class="col-md-4">
                                {{ Form::label('label','Part Name')}}
                                {{Form::text('partname','',['class' => 'form-control','placeholder' => 'Part Name'])}}
                            </div>
                            <div class="col-md-4">
                                {{ Form::label('label','Drawing No')}}
                                {{Form::text('drawing','',['class' => 'form-control','placeholder' => 'Drawing No'])}}
                            </div>
                            <div class="col-md-4">
                                {{ Form::label('label','Rev')}}
                                {{Form::text('rev','',['class' => 'form-control','placeholder' => 'Rev'])}}       
                            </div>
                    </div>
                    <br>
                    <div class="row">
                            <div class="col-md-4">
                                {{ Form::label('label','Order Qty')}}
                                {{Form::text('orderqty','',['class' => 'form-control','placeholder' => 'Order Qty'])}}
                            </div>
                            <div class="col-md-4">
                                    {{ Form::label('label','Stock Balance')}}
                                    {{Form::text('stockbalance','',['class' => 'form-control','placeholder' => 'Stock Balance'])}}
                            </div>
                            <div class="col-md-4">
                                    {{ Form::label('label','Open Date')}}
                                    <div class="ui calendar example2" id="example2">
                                    <div class="ui input left icon">
                                        <i class="calendar icon"></i>
                                        <input type="text" name="opendate" placeholder="Date" style="width:325px">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-4">
                                {{ Form::label('label','Due Date')}}
                                <div class="ui calendar example2" id="example2">
                                    <div class="ui input left icon">
                                        <i class="calendar icon"></i>
                                        <input type="text" name="duedate" placeholder="Date" style="width:325px">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                    {{ Form::label('label','Material')}}
                                    {{Form::text('material','',['class' => 'form-control','placeholder' => 'Material'])}}
                            </div>
                            <div class="col-md-4">
                                    
                            </div>
                        </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <span class="switch switch-sm">
                                <input type="checkbox" class="switch" id="switch-sm" onclick="myFunction()">
                                <label for="switch-sm">Advance Serach</label>
                            </span>
                        </div>
                    </div>
                    <div class="advance">
                        <div class="row">
                            <div class="col-md-3">
                                {{ Form::label('label','Status Process')}}
                                {{Form::text('statusporcess','',['class' => 'form-control','placeholder' => 'Status Process'])}}
                            </div>
                            <div class="col-md-3">
                                {{ Form::label('label','Part Status')}}
                                {{Form::text('partstatus','',['class' => 'form-control','placeholder' => 'Part Status'])}}
                            </div>
                            <div class="col-md-3">
                                {{ Form::label('label','Machine No')}}
                                {{Form::text('machineno','',['class' => 'form-control','placeholder' => 'Machine No'])}}
                            </div>
                            <div class="col-md-3">
                                {{ Form::label('label','Tech Name')}}
                                {{Form::text('tectname','',['class' => 'form-control','placeholder' => 'Tech Name'])}}
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="ui buttons">
                            {{Form::submit('Search',['class' => 'ui primary  button'])}}
                            <div class="or"></div>
                            </div>
                            {{Form::reset('Clear',['class' => 'ui  button'])}}
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
                <div class="card-body card border-secondary  mb-3 col-md-12">
					<table class="table table-bordered table-striped dataTables_wrapper" id="table_data">
					        <thead class="btn-secondary" >
					          <tr width=100% style="text-align:center">
                                <th style="width:100px">Action</th>
                                <th style="width:200px">Customer Name</th>
                                <th style="width:100px">P/O Number</th>
                                <th style="width:100px">Job Number</th>
                                <th style="width:50px">Item</th>
                                <th style="width:100px">Job Date</th>
                                <th style="width:100px">Drawing No</th>
                                <th style="width:100px">Rev</th>
                                <th style="width:100px">Part Name</th>
                                <th style="width:80px">Order Qty</th>
                                <th style="width:120px">Stock Balance</th>
                                <th style="width:80px">Material</th>
                                <th style="width:100px">Due Data</th>
                                <th style="width:80px">Over Due</th>
                                <th style="width:120px">Status Process</th>
                                <th style="width:100px">Machine No</th>
                                <th style="width:120px">Part Status</th>
                                <th style="width:100px">Remark</th>
					          </tr>
					        </thead>
					        <tbody>
                                @if (isset($result))
                                @foreach ($result as $value)
                                    <tr>
                                        <td style="text-align: center;">{{ Html::link('search/show/'.base64_encode($value->job_no_job.'@'.$value->item_fullname), 'Select', array('class'=> 'btn btn-secondary')) }}</td>
                                        <td>{{ $value->cust_name }}</td>
                                        @if (isset($value->po_no))
                                            <td>{{ $value->po_no }}</td>
                                        @else
                                            <td style="text-align:center;">{{ '-' }}</td>
                                        @endif
                                        <td>{{ $value->job_no_job }}</td>
                                        <td>{{ $value->item_fullname }}</td>
                                        <td>{{ $value->job_date }}</td>
                                        <td>{{ $value->drawing_no }}</td>
                                        <td>{{ $value->rev }}</td>
                                        <td>{{ $value->discirption_1 }}</td>
                                        <td>{{ $value->order_qty }}</td>
                                        <td>{{ $value->qty_set }}</td>
                                        <td>{{ $value->material }}</td>
                                        <td>{{ $value->duedate_po }}</td>
                                        <td></td>
                                        @php
                                            $status_show = array(
                                                '1' => 'WorkIn Process',
                                                '2' => 'Finish',
                                                '3' => 'Cancle'
                                            );
                                            if ($value->job_status != '') {
                                                foreach ($status_show as $key => $val) {
                                                    if ($value->job_status == $key) {
                                                            $newstatus = $val;
                                                    }
                                                }
                                            }
                                        @endphp
                                        <td>{{ $newstatus }}</td>
                                        <td></td>
                                        <td>{{ $value->item_status }}</td>
                                        <td>{{ $value->remark }}</td>
                                    </tr>
                                @endforeach
                                @endif
					        </tbody>
					</table>
				</div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    window.onload = function() {
        var x = document.getElementsByClassName("advance");
        x[0].style.display = "none";
  };
    function myFunction() {
    var x = document.getElementsByClassName("advance");
    if (x[0].style.display === "none") {
        x[0].style.display = "block";
    } else {
        x[0].style.display = "none";
    }
}
</script>