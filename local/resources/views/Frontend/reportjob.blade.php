@include('layouts.menu')
<style>
    .but{
	height: 40px;
    width: 81px;
	}
</style>
<body>
<br>
<div class="container">
    <div class="content">
        <div class="card">
            <div class="card-header bg-secondary text-white">
                <div class="float-left" style="font-size:20px;">JobOrder Report</div>
                {{-- <div class="float-right">
                    <button type="button" class="btn btn-dark">Exit</button>
                </div> --}}
            </div>
            <div class="card-body row">
            <div class="col-md-6">
                <div class="card-header bg-info text-white">
                    Between Date
                </div>
                {{Form::open(['url'=>'report/between','enctype'=>'multipart/form-data'])}}
                <div class="card-body card border-secondary mb-3">
                    <div class="row">
                        <div class="col-md-6">
                            {{ Form::label('label','Date Type')}}
								<select class="form-control" name="date_type">
                                <option selected value="">ประเภทวันที่</option>
                                <option value="1">Received P/O Date</option>
                                <option value="2">Invoice Date</option>
                                <option value="3">Job Date</option>
                                <option value="4">Due Date</option>
                                <option value="5">Finish Date</option>
                                </select>
                        </div>
                        <div class="col-md-6">
                            {{ Form::label('label','ช่วงวันที่')}}
                                <div class="ui calendar example2" id="example2">
                                    <div class="ui input left icon">
                                        <i class="calendar icon"></i>
                                        <input type="text" name="date_1" placeholder="Date" style="width:225px">
                                    </div>
                                </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            {{ Form::label('label','Job Status')}}
								<select class="form-control" name="status">
								<option selected value="">ประเภทสถานะ</option>
                                <option value="1">WorkInProcess</option>
                                <option value="2">Fisnish</option>
                                <option value="3">Cancel</option>
                                </select>
                        </div>
                        <div class="col-md-6">
                            {{ Form::label('label','ถึงวันที่')}}
                                <div class="ui calendar example2" id="example2">
                                    <div class="ui input left icon">
                                        <i class="calendar icon"></i>
                                        <input type="text" name="date_2" placeholder="Date" style="width:225px">
                                    </div>
                                </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-secondary">Search Report</button>
                        </div>
                    </div>
                </div>
            {{Form::close()}}
            </div>
            <div class="col-md-6">
                <div class="card-header bg-info text-white">
                    Select Customer
                </div>
                {{Form::open(['url'=>'','enctype'=>'multipart/form-data'])}}
                <div class="card-body card border-secondary mb-3">
                    <div class="row float-left">
                            <div class="col-md-12">
                                {{ Form::label('label','Customer')}}
                                {{Form::text('Text','',['class' => 'form-control','placeholder' => 'Customer'])}}
                            </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            {{ Form::label('label','Date Type')}}
								<select class="form-control">
								<option selected value="">ประเภทวันที่</option>
                                <option value="1">Received P/O Date</option>
                                <option value="2">Invoice Date</option>
                                <option value="3">Job Date</option>
                                <option value="4">Due Date</option>
                                <option value="5">Finish Date</option>
                                </select>
                        </div>
                        <div class="col-md-6">
                            {{ Form::label('label','ช่วงวันที่')}}
                                <div class="ui calendar example2" id="example2">
                                    <div class="ui input left icon">
                                        <i class="calendar icon"></i>
                                        <input type="text" placeholder="Date" style="width:225px">
                                    </div>
                                </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            {{ Form::label('label','Status Type')}}
								<select class="form-control">
								<option selected value="">ประเภทสถานะ</option>
                                <option value="1">WorkInProcess</option>
                                <option value="2">Fisnish</option>
                                <option value="3">Cancel</option>
                                </select>
                        </div>
                        <div class="col-md-6">
                            {{ Form::label('label','ถึงวันที่')}}
                                <div class="ui calendar example2" id="example2">
                                    <div class="ui input left icon">
                                        <i class="calendar icon"></i>
                                        <input type="text" placeholder="Date" style="width:225px">
                                    </div>
                                </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <button type="button" class="btn btn-secondary">Search Report</button>
                        </div>
                    </div>
                </div>
                {{Form::close()}}
            </div>
            </div>
        </div>
    </div>
</div>
</body>