<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
<head>
    <style>
        table, td, th{
            padding: 5px; 
            padding-left: 10px;
            padding-right: 10px;
            border-collapse: collapse;
        }
        .container { 
            max-width: 1300px; 
        }
        .hja_tr{
            height: 50px;
        }
    </style>
</head>
<script type="text/javascript">
     window.print();
</script>
<body>
<br>
<div class="container">
    <div class="content">
        <div class="row" style="padding-bottom: 10px;">
            <div class="pull-left">
                <label for="">Title : Job Assignment</label>
            </div>
            <div class="text-center" style="position: relative;left: 200px;">
                <h3>Happy Day Engineering Co.,Ltd.</h3>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div style="padding-bottom: 20px;width:100%;">
                <table border="1px" style="width:100%" >
                    <tr class="hja_tr">
                        <td style="width: 16%;">Jobnumber:{{ '&nbsp; '.$job[0]->job_no }}</td>
                        <td style="width: 9%;text-align:center;">{{ $job[0]->item_fullname }}</td>
                        <td style="width: 15%;" colspan="2"></td>
                        <td style="width: 20%;">P/O no:{{ '&nbsp; '.$job[0]->po_no }}</td>
                        <td style="width: 20%;">Due Date:{{ '&nbsp; '.$job[0]->duedate_po }}</td>
                    </tr>
                    <tr class="hja_tr">
                        <td style="width: 25%;" colspan="2">Drawing no:{{ '&nbsp; '.$job[0]->drawing_no }}</td>
                        <td style="width: 15%;" >Revision:{{ '&nbsp; '.$job[0]->rev }}</td>
                        <td style="width: 17%;text-align:center;"><span class="float-left">Qty:</span>{{ '&nbsp;'.$job[0]->order_qty }}<span class="float-right" style="font-size:12px;padding-top:5px"> Pcs.</span></td>
                        <td style="width: 20%;">Part Description:{{ '&nbsp; '.$job[0]->discirption_1 }}</td>
                        <td style="width: 20%;">Description 2:{{ '&nbsp; '.$job[0]->discirption_2 }}</td> 
                    </tr>
                    <tr class="hja_tr">
                        <td style="width: 25%;" colspan="2">Materail:{{ '&nbsp; '.$job[0]->material }}</td>
                        <td style="width: 15%;text-align:center;" ><span class="float-left">Hardness:</span>{{ '&nbsp; '.$job[0]->hardness }}<span class="float-right" style="font-size:12px;padding-top:5px"> Hrc.</span></td>
                        <td style="width: 17%;text-align:center;"><img src="data:image/png;base64,{{DNS1D::getBarcodePNG($job[0]->job_no, 'C39+',true)}}" alt="barcode" /></td>
                        <td style="width: 20%;">Assign Date:{{ '&nbsp; '.$job[0]->assign_date }}</td>
                        <td style="width: 20%;">Assign By:{{ '&nbsp; '.$job[0]->assign_by }}</td>
                    </tr>
                </table>
            </div>
            <div style="width:100%;">
                <table border="1px" style="width:100%;text-align: center;">
                    <tr>
                        <th style="width:2%" rowspan="2">No</th>
                        <th style="width:17%" colspan="2" rowspan="2">Precess</th>
                        <th style="width:5%" rowspan="2">Plan Hour</th>
                        <th style="width:5%" rowspan="2">Start Date</th>
                        <th style="width:5%" rowspan="2">Start Time</th>
                        <th style="width:5%" rowspan="2">Finish Date</th>
                        <th style="width:5%" rowspan="2">Finish Time</th>
                        <th style="width:5%" rowspan="2">Used Hour</th>
                        <th style="width:10%" colspan="2">Ouantity</th>
                        <th style="width:10%" rowspan="2">TechName</th>
                        <th style="width:16%" rowspan="2">Reason</th>
                        <th style="width:15%" colspan="3">One Time Judgs</th>
                    </tr>
                    <tr>
                        <td>Receive</td>
                        <td>Finished</td>
                        <td>Yes</td>
                        <td>No</td>
                        <td>Action</td>
                    </tr>
                    @php
                        $title1 = array(
                            'Vender' => 'Vender' , 
                            'Preform QC' => 'Preform QC' ,
                            'W/C Blank' => 'W/C Blank' ,
                        );
                    @endphp
                    @foreach ($title1 as $item)
                        <tr>
                            <td></td>
                            <td colspan="2">{{ $item }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endforeach
                    @php
                        $title2 = array(
                            'Milling' => 'Milling' , 
                            'CNC' => 'CNC' ,
                            'Lathe' => 'Lathe' ,
                        );
                    @endphp
                    @foreach ($title2 as $value)
                        <tr>
                            <td rowspan="2"></td>
                            <td colspan="2" rowspan="2" style="height:55px">{{ $value }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endforeach
                    @php
                        $title3 = array(
                            'QC' => 'QC' , 
                            'Heat Treat' => 'Heat Treat' ,
                            'Grinding-1' => 'Grinding-1' ,
                            'Super Dril' => 'Super Dril' ,
                        );
                    @endphp
                    @foreach ($title3 as $title3)
                        <tr>
                            <td></td>
                            <td colspan="2">{{ $title3 }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endforeach
                    <tr>
                        <td rowspan="2"></td>
                        <td rowspan="2">Wire Cut</td>
                        <td>WC 1</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>WC 2</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @php
                        $title4 = array(
                            'QC - Wire Cut' => 'QC - Wire Cut' , 
                            'Grinding - 2' => 'Grinding - 2' ,
                        );
                    @endphp
                    @foreach ($title4 as $title4)
                        <tr>
                            <td></td>
                            <td colspan="2">{{ $title4 }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endforeach
                    <tr>
                        <td rowspan="2"></td>
                        <td rowspan="2">EDM</td>
                        <td>EDM 1</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>EDM 2</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">Grinding - 3</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td rowspan="2"></td>
                        <td rowspan="2">Profile Grinding</td>
                        <td>PG 1</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PG 2</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @php
                        $title5 = array(
                            'Grinding - 4' => 'Grinding - 4' , 
                            'MarkLaser' => 'MarkLaser' ,
                        );
                    @endphp
                    @foreach ($title5 as $title5)
                        <tr>
                            <td></td>
                            <td colspan="2">{{ $title5 }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endforeach
                    <tr>
                        <td rowspan="2"></td>
                        <td rowspan="2">Lap/Polish</td>
                        <td>L 1</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>L 2</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">Coating</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td rowspan="2"></td>
                        <td rowspan="2">QC</td>
                        <td>QC 1</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>QC 2</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">Packing</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <br>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            นโยบายคุณภาพ "มุ่งมั่นพัฒนาการผลิตสินค้าและลูกค้าพึงพอใจ อย่างต่อเนื่อง"
        </div>
        <div class="col-md-3">
            <span style="padding-left:20px">
                *Action = (I)nterview,(T)eaching,(W)arning *
            </span>
        </div>
        <div class="col-md-4">
            <span class="float-right">
                03-07-60 FO-QP-PD.1-02 (Ver.06)
            </span>
        </div>
    </div>
    <br>
</div>
</body>