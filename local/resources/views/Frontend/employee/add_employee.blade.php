@include('layouts.menu')
<head>

</head>
<body>
    <div class="container">
        <br>
        <div><h2>ระบบเพิ่มพนักงาน</h2></div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card">
            <div class="card-header bg-secondary text-white">
                @if (isset($employee))
                    Edit Form
                @else
                    Create Form
                @endif
                <div class="float-right">
                    <a class="" href="{{url('admin/add')}}" style="padding-right:5px">
                        <button type="button" class="btn btn-secondary btn-sm">เพิ่มข้อมูลพนักงาน</button>
                    </a>
                    <a class="" href="{{url('admin/list')}}" style="">
                        <button type="button" class="btn btn-secondary btn-sm">จัดการข้อมูลพนักงาน</button>
                    </a>
                </div>
                {{-- <div class="row">
                    <div class="col-md-4">
                        @if (isset($employee))
                        <p style="padding-top:8px">Edit Form</p>
                        @else
                        <p style="padding-top:8px">Create Form</p>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <div class="float-right">
                            <a class="" href="{{url('admin/add')}}" style="padding-right:5px">
                                <button type="button" class="btn btn-primary">เพิ่มข้อมูลพนักงาน</button>
                            </a>
                            <a class="" href="{{url('admin/list')}}" style="">
                                <button type="button" class="btn btn-primary">จัดการข้อมูลพนักงาน</button>
                            </a>
                        </div>
                    </div>
                </div> --}}
                
            </div>
            <div class="card-body">
                <div class="card-body card border-secondary mb-3">
                    @if (isset($employee))
                        {{Form::open(['url'=>['admin/update',$employee->id],'enctype'=>'multipart/form-data'])}}
                    @else
                        {{Form::open(['url'=>'admin/store','enctype'=>'multipart/form-data'])}}
                    @endif
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">ชื่อ</label>
                            @if (isset($employee))
                                {{Form::text('name',$employee->name,['class' => 'form-control','placeholder' => 'ชื่อพนักงาน','required'])}}
                            @else
                                {{Form::text('name','',['class' => 'form-control','placeholder' => 'ชื่อพนักงาน','required'])}}
                            @endif
                        </div>
                        <div class="col-md-4">
                            <label for="">นามสกุล</label>
                            @if (isset($employee))
                                 {{Form::text('lastname',$employee->lastname,['class' => 'form-control','placeholder' => 'นามสกุลพนักงาน','required'])}}
                            @else
                                {{Form::text('lastname','',['class' => 'form-control','placeholder' => 'นามสกุลพนักงาน','required'])}}
                            @endif
                        </div>
                        <div class="col-md-4">
                            <label for="">ชื่อภาษาอังกฤษ</label>
                            @if (isset($employee))
                                {{Form::text('nameeng',$employee->name_eng,['class' => 'form-control','placeholder' => 'ชื่อภาษาอังกฤษ','required'])}}
                            @else
                                {{Form::text('nameeng','',['class' => 'form-control','placeholder' => 'ชื่อภาษาอังกฤษ','required'])}}
                            @endif
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">รหัสพนักงาน</label>
                            @if (isset($employee))
                                {{Form::text('code',$employee->code,['class' => 'form-control','placeholder' => 'รหัสพนักงาน','required'])}}
                            @else
                                {{Form::text('code','',['class' => 'form-control','placeholder' => 'รหัสพนักงาน','required'])}}
                            @endif
                        </div>
                        @php
                            $depart = array(
                                'FG'=>'FG',
                                'HD2/FG'=>'HD2/FG',
                                'EDM'=>'EDM',
                                'SL'=>'SL'
                            );
                        @endphp
                        <div class="col-md-4">
                            <label for="">แผนก</label>
                            @if (isset($employee))
                                <select class="form-control" name="department">  selected="selected"
                                        @foreach ($depart as $value)
                                            @if ($employee->department == $value)
                                                <option selected="selected" value={{ $value }}>{{ $value }}</option>
                                            @else
                                                <option value={{ $value }}>{{ $value }}</option>
                                            @endif
                                        @endforeach
                                </select>
                            @else
                                <select class="form-control" name="department">
                                <option selected="selected" value>เลือกแผนก</option>
                                @foreach ($depart as $value)
                                    <option value={{ $value }}>{{ $value }}</option>
                                @endforeach
                                </select>
                            @endif
                            
                        </div>
                        <div class="col-md-4">
                            <label for="">วันเริ่มทำงาน</label>
                                <div class="ui calendar example2" id="example2">
                                    <div class="ui input left icon">
                                        <i class="calendar icon"></i>
                                        @if (isset($employee))
                                            <input type="text" value="{{$employee->start_work}}" placeholder="วันเริ่มทำงาน" style="width:320px;" name="datestart">
                                        @else
                                            <input type="text" placeholder="วันเริ่มทำงาน" style="width:320px;" name="datestart">
                                        @endif
                                        
                                    </div>
                                </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="ui buttons">
                            {{Form::submit('Save',['class' => 'ui primary  button'])}}
                            <div class="or"></div>
                            </div>
                            {{Form::reset('Clear',['class' => 'ui  button'])}}
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
</body>