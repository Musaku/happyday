@include('layouts.menu')
<head> 
    @if (isset($not))
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        @if ($not == 1)
            <script type="text/javascript">
                swal({
                        title: "รหัสผ่านของคุณไม่ถูกต้อง",
                        text: "กรุณากรอกรหัสผ่านอีกครั้ง",
                        icon: "warning",
                        dangerMode: true,
                    })
            </script>
        @else
            <script type="text/javascript">
                swal({
                        title: "รหัสผ่านใหม่ของคุณไม่ตรงกัน",
                        text: "กรุณากรอกรหัสผ่านอีกครั้ง",
                        icon: "warning",
                        dangerMode: true,
                    })
            </script>
        @endif
    @endif
</head>
<body>
    <div class="container">
        <br>
        <div><h2>ระบบเปลี่ยยรหัสผ่าน</h2></div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card">
            <div class="card-header bg-secondary text-white">
                Reset Password
                <div class="float-right">
                    <a class="" href="{{url('admin/add')}}" style="padding-right:5px">
                        <button type="button" class="btn btn-secondary btn-sm">เพิ่มข้อมูลพนักงาน</button>
                    </a>
                    <a class="" href="{{url('admin/list')}}" style="">
                        <button type="button" class="btn btn-secondary btn-sm">จัดการข้อมูลพนักงาน</button>
                    </a>
                </div>
                {{-- <div class="row">
                    <div class="col-md-4">
                        <p style="padding-top:8px">Reset Password</p>
                    </div>
                    <div class="col-md-8">
                        <div class="float-right">
                            <a class="" href="{{url('admin/add')}}" style="padding-right:5px">
                                <button type="button" class="btn btn-primary">เพิ่มข้อมูลพนักงาน</button>
                            </a>
                            <a class="" href="{{url('admin/list')}}" style="">
                                <button type="button" class="btn btn-primary">จัดการข้อมูลพนักงาน</button>
                            </a>
                        </div>
                    </div>
                </div> --}}
                    
            </div>
            <div class="card-body">
                <div class="card-body card border-secondary mb-3">
                        {{Form::open(['url'=>['admin/resetpass2',$employee->id],'enctype'=>'multipart/form-data'])}}
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">Old Password</label>
                                {{Form::password('oldpass',['class' => 'form-control','placeholder' => 'Old Password','required'])}}
                        </div>
                        <div class="col-md-4">
                            <label for="">New Password</label>
                                {{Form::password('newpass',['class' => 'form-control','placeholder' => 'New Password','required'])}}
                        </div>
                        <div class="col-md-4">
                            <label for="">Confirm Password</label>
                            {{Form::password('conpass',['class' => 'form-control','placeholder' => 'Confirm Password','required'])}}
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="ui buttons">
                            {{Form::submit('Reset',['class' => 'ui primary  button'])}}
                            <div class="or"></div>
                            </div>
                            {{Form::reset('Clear',['class' => 'ui  button'])}}
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
</body>