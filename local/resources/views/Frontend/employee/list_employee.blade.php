@include('layouts.menu')
<head>

</head>
<body>
     <br>
    <div class="container">
        <div><h2>ระบบจัดการพนักงาน</h2></div>
        <div class="card">
            <div class="card-header bg-secondary text-white">
                รายการพนักงาน
                <div class="float-right">
                    <a class="" href="{{url('admin/add')}}" style="padding-right:5px">
                        <button type="button" class="btn btn-secondary btn-sm">เพิ่มข้อมูลพนักงาน</button>
                    </a>
                    <a class="" href="{{url('admin/list')}}" style="">
                        <button type="button" class="btn btn-secondary btn-sm">จัดการข้อมูลพนักงาน</button>
                    </a>
                </div>
                {{-- <div class="row">
                    <div class="col-md-4">
                        <p style="padding-top:8px">รายการพนักงาน</p>
                    </div>
                    <div class="col-md-8">
                        <div class="float-right">
                            <a class="" href="{{url('admin/add')}}" style="padding-right:5px">
                                <button type="button" class="btn btn-primary">เพิ่มข้อมูลพนักงาน</button>
                            </a>
                            <a class="" href="{{url('admin/list')}}" style="">
                                <button type="button" class="btn btn-primary">จัดการข้อมูลพนักงาน</button>
                            </a>
                        </div>
                    </div>
                </div> --}}
                
            </div>
            <div class="card-body">
                {{-- <div class="card-body card border-secondary mb-3"> --}}
                    <div class="row" style="padding-left: 8px;">
                        <table class="table table-bordered table-striped" id="table_data">
                            <thead class="btn-secondary" >
                            <tr>
                                <th width="5%">No</th>
                                <th width="12%">ชื่อ</th>
                                <th width="12%">นามสกุล</th>
                                {{-- <th width="20%">ชื่อภาษาอังกฤษ</th> --}}
                                <th width="15%">รหัสพนักงาน</th>
                                <th width="10%">แผนก</th>
                                <th width="15%">วันเริ่มงาน</th>
                                <th width="20%" aling="center">Action</th>
                            </tr>
                            </thead>
                            @php
                            $i =1;
                            @endphp
                            <tbody>
                            @foreach ($employee as $data)
                                @if (isset($data))
                                <tr>
                                    <td align="center">{{ $i}}</td>
                                    <td>{{ $data['name']}}</td>
                                    <td>{{ $data['lastname']}}</td>
                                    {{-- <td>{{ $data['name_eng']}}</td> --}}
                                    <td>{{ $data['code']}}</td>
                                    <td>{{ $data['department']}}</td>
                                    <td>{{ $data['start_work']}}</td>
                                    <td align="center">
                                    {{ Form::open(['url' => ['admin/destroy',$data['id']] ]) }}
                                        {{ Html::link('admin/resetpass/'.$data['id'], 'Reset Password', array('class'=> 'btn btn-secondary')) }}
                                        {{ Html::link('admin/edit/'.$data['id'], 'Edit', array('class'=> 'btn btn-secondary')) }}
                                        {{ Form::submit('Delete',array('class' => 'btn btn-secondary')) }}
                                    {{ Form::close() }}
                                    </td>
                                </tr>
                                @else
                                <td colspan="6">no data</td>
                                @endif
                                @php
                                $i++;
                                @endphp
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                {{-- </div> --}}
            </div>
        </div>
    </div>
</body>