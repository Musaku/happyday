@include('layouts.menu')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<style>
    table,
    td,
    th {
        padding: 5px;
        padding-left: 10px;
        padding-right: 10px;
        border-collapse: collapse;
    }
    .container {
        max-width: 1300px;
    }
</style>
<body>
    <br>
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-md-12" style="padding-left: 45%;"><h3>FINISH GOOD</h3></div>
            </div>
            <div class="row">
                <div class="col-md-4">
                </div>
                <div class="col-md-8" style="padding-left: 100px;">
                    <h3>Happy Day Engineering Co.,Ltd.</h3>
                </div>
            </div>
            <br>
            <div>
                <table style="width:100%" border="1px">
                    <tr style="text-align:center;">
                        <th style="width:7%">Job No.</th>
                        <th style="width:3%">Item</th>
                        <th style="width:8%">Job Open Date</th>
                        <th style="width:15%">Drawing No.</th>
                        <th style="width:3%">Rev.</th>
                        <th style="width:10%">Description</th>
                        <th style="width:3%">Qty.</th>
                        <th style="width:3%">U/M</th>
                        <th style="width:8%">P/O Number</th>
                        <th style="width:5%">Due Original</th>
                        <th style="width:10%">Finish Date</th>
                        <th style="width:25%">Remark</th>
                    </tr>
                    <tr>
                        <td colspan="12">
                            <label for="" style="padding-top:5px;">Company : Orange Thailand</label>
                        </td>
                    </tr>
                    @for ($i = 0; $i < 20; $i++)
                        <tr style="text-align:center;">
                            <td style="width:7%">test</td>
                            <td style="width:3%">test</td>
                            <td style="width:8%">test</td>
                            <td style="width:15%">test</td>
                            <td style="width:3%">test.</td>
                            <td style="width:10%">test</td>
                            <td style="width:3%">test</td>
                            <td style="width:3%">test</td>
                            <td style="width:8%">test</td>
                            <td style="width:5%">test</td>
                            <td style="width:10%">test</td>
                            <td style="width:25%">test</td>
                        </tr>
                    @endfor
                </table>
                <br>
            </div>
        </div>
    </div>
</body>
</html>