@extends('layouts.main')
<link rel="stylesheet" href="{{ asset('css/_all-skins.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/adminlte.min.js') }}"></script>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  @include('layouts.head_admin')
  @include('layouts.menuleft')
  <div class="content-wrapper">
      <br>
    <div class="container">
        <div class="card">
            <div class="card-header bg-secondary text-white">
                รายการพนักงาน
            </div>
            <div class="card-body">
                {{-- <div class="card-body card border-secondary mb-3"> --}}
                    <div class="row" style="padding-left: 8px;">
                        <table class="table table-bordered table-striped" id="table_data">
                            <thead class="btn-secondary" >
                            <tr>
                                <th width="5%">No</th>
                                <th width="10%">ID CUT</th>
                                <th width="14%">Customer NO</th>
                                <th width="20%">Customer Name</th>
                                <th width="10%">Tel</th>
                                <th width="10%">Fax</th>
                                <th width="20%" aling="center">Action</th>
                            </tr>
                            </thead>
                            @php
                            $i =1;
                            @endphp
                            <tbody>
                            @foreach ($customer as $data)
                                @if (isset($data))
                                <tr>
                                    <td align="center">{{ $i}}</td>
                                    <td>{{ $data['id_cus']}}</td>
                                    <td>{{ $data['cus_no']}}</td>
                                    <td>{{ $data['cus_name']}}</td>
                                    <td>{{ $data['fax']}}</td>
                                    <td>{{ $data['tel']}}</td>
                                    <td align="center">
                                    {{ Form::open(['url' => ['customer/destroy',$data['id']] ]) }}
                                        {{ Html::link('customer/view/'.$data['id'], 'View', array('class'=> 'btn btn-secondary')) }}
                                        {{ Html::link('customer/edit/'.$data['id'], 'Edit', array('class'=> 'btn btn-secondary')) }}
                                        {{ Form::submit('Delete',array('class' => 'btn btn-secondary')) }}
                                    {{ Form::close() }}
                                    </td>
                                </tr>
                                @else
                                <td colspan="6">no data</td>
                                @endif
                                @php
                                $i++;
                                @endphp
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                {{-- </div> --}}
            </div>
        </div>
    </div>
  </div>
  <footer class="main-footer">
    
  </footer>
  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
