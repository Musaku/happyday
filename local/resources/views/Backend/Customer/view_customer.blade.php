@extends('layouts.main')
<link rel="stylesheet" href="{{ asset('css/_all-skins.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/adminlte.min.js') }}"></script>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <style>
      .light-grey{
          background-color: #f1f1f1;
      }
      table, td, th {
        border: 2px solid #adadad;
    }
        .ddc {
        padding-left: 60px;
        position: relative;
        width: 100%;
        min-height: 1px;
        padding-right: 15px;
        position: relative;
        left: 50%;
    }
  </style>    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  @include('layouts.head_admin')
  @include('layouts.menuleft')
  <div class="content-wrapper">
      <br>
      <div class="container">
        <div class="card">
            <div class="card-header bg-secondary text-white">
                รายละเอียดลูกค้า
            </div>
            <div class="card-body">
                {{-- <div class="row cen">
                    <div class="col-md-3 card-body card border-secondary mb-3">
                        <p>ID Customer</p>
                    </div>
                     <div class="col-md-3 card-body card border-secondary mb-3 ">
                        <p>{{ $customer->id_cus }}</p>
                    </div>
                </div> --}}
                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="col-12 col-md-12 ddc">
                            <h4>ข้อมูลเพิ่มเติม</h4>
                            <table class="table">
                                <tr>
                                    <td class="light-grey" width="40%">ID Customer</td>
                                    <td width="60%">{{ $customer->id_cus }}</td>
                                </tr>
                                <tr>
                                    <td class="light-grey">Customer No</td>
                                    <td>{{ $customer->cus_no }}</td>
                                </tr>
                                <tr>
                                    <td class="light-grey">Customer Name</td>
                                    <td>{{ $customer->cus_name }}</td>
                                </tr>
                                <tr>
                                    <td class="light-grey">Tel</td>
                                    <td>{{ $customer->tel }}</td>
                                </tr>
                                <tr>
                                    <td class="light-grey">Fax</td>
                                    <td>{{ $customer->fax }}</td>
                                </tr>
                                <tr>
                                    <td class="light-grey">Customer Address</td>
                                    <td style="max-width:100px">{{ $customer->cus_address }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
  </div>
  <footer class="main-footer">
    
  </footer>
  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
