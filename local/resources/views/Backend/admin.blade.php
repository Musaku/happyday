@extends('layouts.main')
<link rel="stylesheet" href="{{ asset('css/_all-skins.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/adminlte.min.js') }}"></script>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  @include('layouts.head_admin')
  @include('layouts.menuleft')
  <div class="content-wrapper">
    <!-- Main content -->
      
  </div>
  <footer class="main-footer">
    
  </footer>
  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
