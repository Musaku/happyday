@extends('layouts.main')
<link rel="stylesheet" href="{{ asset('css/_all-skins.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/adminlte.min.js') }}"></script>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  @include('layouts.head_admin')
  @include('layouts.menuleft')
  <div class="content-wrapper">
      <br>
    <div class="container">
        <div class="card">
            <div class="card-header bg-secondary text-white">
                รายการพนักงาน
            </div>
            <div class="card-body">
                {{-- <div class="card-body card border-secondary mb-3"> --}}
                    <div class="row" style="padding-left: 8px;">
                        <table class="table table-bordered table-striped" id="table_data">
                            <thead class="btn-secondary" >
                            <tr>
                                <th width="5%">No</th>
                                <th width="12%">ชื่อ</th>
                                <th width="12%">นามสกุล</th>
                                {{-- <th width="20%">ชื่อภาษาอังกฤษ</th> --}}
                                <th width="15%">รหัสพนักงาน</th>
                                <th width="10%">แผนก</th>
                                <th width="15%">วันเริ่มงาน</th>
                                <th width="20%" aling="center">Action</th>
                            </tr>
                            </thead>
                            @php
                            $i =1;
                            @endphp
                            <tbody>
                            @foreach ($employee as $data)
                                @if (isset($data))
                                <tr>
                                    <td align="center">{{ $i}}</td>
                                    <td>{{ $data['name']}}</td>
                                    <td>{{ $data['lastname']}}</td>
                                    {{-- <td>{{ $data['name_eng']}}</td> --}}
                                    <td>{{ $data['code']}}</td>
                                    <td>{{ $data['department']}}</td>
                                    <td>{{ $data['start_work']}}</td>
                                    <td align="center">
                                    {{ Form::open(['url' => ['admin/destroy',$data['id']] ]) }}
                                        {{-- {{ Html::link('agent/view/'.$data['id'], 'View', array('class'=> 'btn btn-primary')) }} --}}
                                        {{ Html::link('admin/edit/'.$data['id'], 'Edit', array('class'=> 'btn btn-secondary')) }}
                                        {{ Form::submit('Delete',array('class' => 'btn btn-secondary')) }}
                                    {{ Form::close() }}
                                    </td>
                                </tr>
                                @else
                                <td colspan="6">no data</td>
                                @endif
                                @php
                                $i++;
                                @endphp
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                {{-- </div> --}}
            </div>
        </div>
    </div>
  </div>
  <footer class="main-footer">
    
  </footer>
  <div class="control-sidebar-bg"></div>
</div>
</body>
</html>
