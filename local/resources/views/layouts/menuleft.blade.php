<style>
    .pad{
        padding-left: 10px;
    }
</style>
<aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">เมนู</li>

        <li class="treeview ">
          <a href="#">
            <i class="fas fa-address-card"></i> <span class="pad">ระบบจัดการพนักงาน</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="{{url('admin/add')}}"><i class="far fa-circle"></i><span class="pad">เพิ่มพนักงาน</span></a></li>
            <li><a href="{{url('admin/list')}}"><i class="far fa-circle"></i><span class="pad">จัดการพนักงาน</span></a></li>
          </ul>
        </li>
        <li class="treeview ">
          <a href="#">
            <i class="fas fas fa-users"></i> <span class="pad">ระบบจัดการลูกค้า</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="{{url('customer/add')}}"><i class="far fa-circle"></i><span class="pad">เพิ่มลูกค้า</span></a></li>
            <li><a href="{{url('customer/list')}}"><i class="far fa-circle"></i><span class="pad">จัดการลูกค้า</span></a></li>
          </ul>
        </li>

        {{-- <li class="treeview ">
          <a href="#">
            <i class="far fa-file-excel"></i> <span class="pad">Import Excel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('excel/importExport')}}"><i class="far fa-circle"></i><span class="pad">Import Excel</span></a></li>
          </ul>
        </li>

        <li class="treeview ">
          <a href="#">
            <i class="fas fa-sign-out-alt"></i> <span class="pad">Logout</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('logout')}}"><i class="far fa-circle"></i><span class="pad">Logout</span></a></li>
          </ul>
        </li> --}}
      </ul>
    </section>
  </aside>