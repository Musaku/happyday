@extends('layouts.main')
<head>
  <title>Happyday</title>
  <link rel="SHORTCUT ICON" href="{{ asset('img/favicon1.ico') }}"/>
  <style type="text/css">
    
  </style>
</head>
<nav class="navbar navbar-expand-lg navbar-light nav_hd" >
  <a class="navbar-brand" href="{{url('/')}}"><img class="menu_logohd" src="{{asset('img/logo_1.png')}}"></a>
  {{-- <button class="navbar-toggler" type="button" 
  data-toggle="collapse" data-target="#navbarSupportedContent" 
  aria-controls="navbarSupportedContent" aria-expanded="false"
   aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button> --}}

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item ">
        <a class="nav-link" href="{{url('joborder')}}" style="">job Order</a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="{{url('searchmain')}}">Search Order</a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="{{url('reportjob')}}">JobOrder Report</a>
      </li>
      {{-- <li class="nav-item ">
        <a class="nav-link" href="#">Menu</a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="#">Menu</a>
      </li> --}}
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Report
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          {{-- <a class="dropdown-item" href="{{ url('report/job') }}">Report Job</a> --}}
          <a class="dropdown-item" href="{{ url('report/work') }}">Report Work</a>
          <a class="dropdown-item" href="{{ url('report/summary_po') }}">Summary P/O Report</a>
          <a class="dropdown-item" href="{{ url('report/summary_finish') }}">Summary Finish P/O Report</a>
          <a class="dropdown-item" href="{{ url('report/finish_good') }}">Finish Good Report</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Management
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="{{ url('customer/list') }}">Customer</a>
          <a class="dropdown-item" href="{{ url('admin/list') }}">Employee</a>
          <a class="dropdown-item" href="{{ url('product/show') }}">Product</a>
        </div>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0" style="padding-right: 24px;">
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu menu_righthd">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="hidden-xs">{{ $name }}</span>
                  </a>
            <ul class="dropdown-menu" style="left: -98%;text-align: center;">
              <li class="user-footer">
                <div class="float-left">
                  <a href="{{url('logout')}}" class="btn btn-default ">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </form>
  </div>
</nav>
