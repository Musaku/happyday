{{-- ///////////////////////CSS ////////////////////--}}

<link rel="stylesheet" type="text/css" href="{{ asset('datatable/jquery.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/all.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/switch.css') }}">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
<link href="https://cdn.rawgit.com/mdehoog/Semantic-UI/6e6d051d47b598ebab05857545f242caf2b4b48c/dist/semantic.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/materialdesignicons.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/style_hd.css') }}">
<link rel="stylesheet" href="{{ asset('css/dropify.min.css') }}">

<style>
      .container{
            max-width: 95%;
      }
</style>

{{-- ///////////////////////JS ////////////////////--}}
{{-- <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}" ></script> --}}
<script src="{{ asset('js/jquery.min.js') }}"></script>
{{-- <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.slim.min.js') }}"></script> --}}
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{ asset('js/datepicker.js') }}"></script>
<script src="https://cdn.rawgit.com/mdehoog/Semantic-UI/6e6d051d47b598ebab05857545f242caf2b4b48c/dist/semantic.min.js"></script>
<script type="text/javascript" src="{{ asset('datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dropify.min.js') }}"></script>

<script type="text/javascript">
  $(document).ready(function(){
     $('#table_data').DataTable( {
        "scrollX": true
        } );
  });
  $(document).ready(function(){
     $('#table_data2').DataTable( {
        } );
  });
//   $(document).ready(function(){
//      $('#table_data3').DataTable( {
//         } );
//   });
</script>
