<?php
// fontend////
Route::get('/','SiteController@index');
Route::get('searchmain','SiteController@searchmain');
Route::get('reportjob','SiteController@reportjob');

// search/////
Route::post('search/main','SearchController@search_order');
Route::get('search/show/{id}','SearchController@show');

// Joborder/////
Route::get('joborder','JoborderController@joborder');
Route::post('joborder/create','JoborderController@create_first');
Route::post('joborder/update/{id}','JoborderController@update_job');
Route::post('joborder/update2/{id}','JoborderController@update_item');
Route::get('serach_cus/{id}','JoborderController@serach_cus');
Route::get('serach_dis/{id}','JoborderController@serach_dis');
Route::get('serach_show','JoborderController@serach_show');
Route::get('joborder/delete_item/{id}','JoborderController@delete_item');
Route::get('joborder/delete/{id}','JoborderController@delete');

// Report///////
Route::get('report/print_job/{id}','ReportController@print_job');
Route::post('report/between','ReportController@between_report');
Route::get('report/job','ReportController@reportjob');
Route::get('report/work','ReportController@reportwork');
Route::get('report/summary_po','ReportController@summary_po');
Route::get('report/summary_finish','ReportController@summary_finish');
Route::get('report/finish_good','ReportController@finish_good');
Route::post('report/jobassign','ReportController@job_as');

///////////// Admin----employee/////////////////
Route::get('admin','AdminController@index');
Route::get('admin/add','AdminController@create');
Route::post('admin/store','AdminController@store');
Route::get('admin/list','AdminController@show');
Route::get('admin/edit/{id}','AdminController@edit');
Route::post('admin/update/{id}','AdminController@update');
Route::post('admin/destroy/{id}','AdminController@destroy');
Route::get('admin/resetpass/{id}','AdminController@editpass');
Route::post('admin/resetpass2/{id}','AdminController@resetpass');

///////////////// Customer/////////////
Route::get('customer/add','CustomerController@create');
Route::post('customer/store','CustomerController@store');
Route::get('customer/list','CustomerController@show');
Route::get('customer/edit/{id}','CustomerController@edit');
Route::post('customer/update/{id}','CustomerController@update');
Route::post('customer/destroy/{id}','CustomerController@destroy');
Route::get('customer/view/{id}','CustomerController@view');

/////////////////////// Product//////////////////

Route::get('product/create','ProductController@create');
Route::post('product/store','ProductController@store');
Route::get('product/show','ProductController@show');
Route::get('product/edit/{id}','ProductController@edit');
Route::post('product/update/{id}','ProductController@update');
Route::get('product/destroy/{id}','ProductController@destroy');
Route::get('list_product','ProductController@list_product');
Route::get('product/excel','ProductController@excel');
Route::post('import_excel','ProductController@import_excel');

// login/////
Route::post('Login/auth','Auth\LoginController@login');
Route::get('logout','Auth\LoginController@logout');

